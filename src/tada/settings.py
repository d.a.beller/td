UMBRELLA_PROJECT_FILENAME = "~/Dropbox/td.txt"

DARK_BACKGROUND = True

DARK_BACKGROUND_COLOR_SEQUENCE = [
    'white',
    'white',
    45,  # light blue
    34,  # medium green
    226,  # gold
    196  # cherry-red
]
LIGHT_BACKGROUND_COLOR_SEQUENCE = [
    'black',
    'black',
    26,  # blue
    28,  # blueish-green
    208,  # orange
    196  # cherry-red
]

DONE_COLOR = 63
NAME_COLOR = 177

JSON_INDENT = 4
TMP_FILENAME = "td_tmp.txt"
BLANK_DATE_STR = "." * 10
BLANK_DESCRIPTION_STR = "_" * 10
WIDTH = 80
MESSAGE_SLEEP_TIME = 1.5  # messages are shown for this many seconds
DONE_SYMBOL = "𝝬"
NOT_DONE_SYMBOL = " "
SOURCE_SYMBOL = "⇐"
LINE_SPACING = 2
DEFAULT_FILE_TYPE = ".txt"  # ".txt" or ".json"