from __future__ import annotations

from markdown import markdown
from html2text import html2text
from .utilities import clipboard_input


class Note:
    def __init__(
        self,
        text: str = "",
        subnotes: None | dict = None,
        selected: bool = False
    ):
        self.selected = selected
        self.text = text
        if subnotes is None:
            subnotes = []
        self.subnotes = [Note(**subnote) for subnote in subnotes]

    def __imul__(self, other: str | tuple | dict) -> Note:
        self.subnotes.append(Note(other))
        return self

    def __repr__(self) -> str:
        ret = self.text
        if len(self.subnotes) > 0:
            ret += "\n"
            ret += str([subnote for subnote in self.subnotes])
        return ret.rstrip("\n")

    def md_formatted(self) -> str:
        ret = "* " + self.text
        for subnote in self.subnotes:
            for line in subnote.md_formatted().splitlines():
                ret += "\n\t" + line
        return ret.rstrip("\n")

    def plaintext_md_formatted(self) -> str:
        return html2text(markdown(self.md_formatted())).rstrip("\n")

    def to_dict(self) -> dict:
        ret = {"text": self.text}
        if len(self.subnotes) > 0:
            ret["subnotes"] = [subnote.to_dict() for subnote in self.subnotes]
        return ret

    def __getitem__(self, n: int) -> Note:
        return self.subnotes[n]

    def __setitem__(self, n: int, value: str | Note):
        if isinstance(value, str):
            value = Note(text=value)
        self.subnotes[n] = value

    def edit(self) -> Note | None:
        old_txt = self.text
        try:
            new_txt = clipboard_input(
                old_text=old_txt,
                old_text_label="old note text: ",
                prompt=        "new note text: "
            )
        except KeyboardInterrupt:
            return
        if isinstance(new_txt, str):
            self.text = new_txt
        return self
