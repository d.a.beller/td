from __future__ import annotations
import asyncio
import calendar
import datetime
import json
import math
import pathlib
import os
import shutil
import subprocess
import sys
from functools import wraps
from time import sleep

from sshkeyboard import listen_keyboard, stop_listening
from blessings import Terminal
from simple_term_menu import TerminalMenu

from .task import Task
from .note import Note
from .utilities import color_func, N_SPACES_INDENT, faint_txt, clipboard_input
from .settings import *
from . import settings
from .hints import HINTS_COLUMN_WIDTH, COLUMN_PAD, HINTS


class Project:
    """ td Project class

    `p.i` to enter interactive mode.
    `p.tasks` to list tasks.
    `p.t` for tree view of Project.
    `p.s` to write to file(s).
    `p.sp` to list subprojects separately.
    `p["<subproject name>"]` to view one subproject.
    `p[<n>]` to get the <n>th Task.
    `p += "<text>"` to add a Task with description <text>.
    """

    def __init__(
        self,
        tasks: (
            str | list[str] | tuple[str] | Task | list[Task] | tuple[Task]
            | None
        )
        = None,
        name: str = "tada",
        filename: str | list[str] | tuple[str] = None,
        description: str = "",
        status: str = "",
        notes: str | None | list[dict] | tuple[dict] = "",
        tags: list[str] | tuple[str] | None = None,
        people: list[str] | tuple[str] | None = None,
        subprojects: list[str] | tuple[str] | None = None,
        sortedby: str = None,
        dark: bool = DARK_BACKGROUND,
        show_completed: bool = True,
        line_spacing: int = LINE_SPACING,
        hints: str = None,
        show_tags: bool = True,
        show_assignments: bool = True,
        show_source: bool = True,
        show_notes_asterisk: bool = True,
        colors: bool = True,
        prompt_style=None,
        history_length_max: int = 1000,
        default_file_type: str = DEFAULT_FILE_TYPE
    ):
        if hints is None:
            hints = HINTS
        if tasks is None:
            tasks = []
        if isinstance(tasks, str):
            tasks = [Task(tasks)]
        if isinstance(tasks, Task):
            tasks = [tasks]
        for _i, task in enumerate(tasks):
            if isinstance(task, str):
                tasks[_i] = Task(task)
        try:
            assert all(isinstance(item, Task) for item in tasks)
        except (TypeError, AssertionError):
            raise TypeError(
                f"Invalid value passed for keyword "
                f"argument \"tasks\""
            )
        self.description = description
        self.status = status
        self.show_notes = True
        self.selected_note_num = 0
        if notes in [None, ""]:
            notes = []
        if isinstance(notes, str):
            notes = [Note(notes)]
        else:
            notes = [Note(**note_dict) for note_dict in notes]
        self.notes = notes
        if len(self.notes) > 0:
            self.notes[self.selected_note_num].selected = True
        if tags is None:
            tags = []
        self.tags = tags
        if people is None:
            people = []
        self.people = people
        self.name = name

        self.selected_task_nums = []
        self.cmd_list = []
        self.subprojects = []
        self._undo_buffer = []
        self._redo_buffer = []
        self.tasks = []
        self.dark = dark
        self.sortedby = sortedby
        self.hints = hints
        for task in tasks:
            if isinstance(task, dict):
                task = Task(**task)
            if isinstance(task, str):
                task = Task(task)
            if isinstance(task, Task):
                self.tasks.append(task)
            elif isinstance(task, Project):
                self.add_subproject(task)
        if isinstance(filename, (list, tuple)):
            subprojects = filename
            filename = None
        if subprojects is None:
            subprojects = []
        for subproject_filename in subprojects:
            try:
                subproject = Project.load(subproject_filename, dark=self.dark)
            except FileNotFoundError:
                pass
            else:
                self.add_subproject(subproject)

        self.default_file_type = default_file_type
        if filename is None:
            filename = (
                self.name.replace(" + ", "+").replace(" ", "_")
                + self.default_file_type
            )
        self.filename = filename

        if self.sortedby is not None:
            self.sort(by=self.sortedby)

        self.show_completed = show_completed
        self.terminal = Terminal()
        self.line_spacing = line_spacing
        self.hints_bar = self.generate_hints_bar()
        self.show_tags = show_tags
        self.show_assignments = show_assignments
        self.show_source = show_source
        self.show_notes_asterisk = show_notes_asterisk
        self.colors = colors

        def _no_style(txt): return txt

        if prompt_style is None:
            prompt_style = faint_txt if self.colors else _no_style
        self.prompt_style = prompt_style
        self.history_length_max = history_length_max
        self.history = []


    def get_width(self):
        width = self.terminal.width
        if width is None:
            width = settings.WIDTH
        return width

    @classmethod
    def from_filenames(
        cls, filenames: list[str] | tuple[str] | None = None
    ) -> Project:
        if filenames is None:
            return cls.get_umbrella()
        else:
            if len(filenames) > 1:
                return cls(filename=filenames)
            elif len(filenames) == 1:
                filename = filenames[0]
                if "." not in filename:
                    filename += self.default_file_type
                return cls.load(filename, create_if_not_found=True)
            else:
                return cls()

    @classmethod
    def get_umbrella(cls) -> Project:
        umbrella_filename = UMBRELLA_PROJECT_FILENAME
        umbrella_filepath = pathlib.Path(umbrella_filename).expanduser()
        umbrella_filename = str(umbrella_filepath)
        if umbrella_filepath.exists():
            umbrella = cls.load(umbrella_filename)
        else:
            umbrella = cls(name="umbrella", filename=umbrella_filename)
            umbrella.save()
        return umbrella

    def add_new_subproject_interactive(self, default_filename="tada"):
        self.terminal.normal_cursor()
        subproject_name = input("sub-Project name: ")
        if len(subproject_name) == 0:
            return
        subproject_filename = input("location: ")
        subproject_filepath = pathlib.Path(subproject_filename)
        if subproject_filepath.is_dir():
            subproject_filepath = subproject_filepath.joinpath(
                default_filename + self.default_file_type
            )
        subproject_filename = str(subproject_filepath)
        subproject = Project(
            name=subproject_name, filename=subproject_filename
        )
        subproject.save()
        self.__iadd__(subproject)
        return subproject

    def append(self, other: Task | Project):
        """
        Append Task, or all Tasks in a Project, to end of `tasks`.

        If `other` is a Project, claim its Tasks (rather than make `other`
        a subproject).
        """
        if isinstance(other, Task):
            self.tasks.append(other.copy())
        elif isinstance(other, Project):
            self.__iadd__(other)
            self.claim(other)

    def reverse(self):
        """ Reverse order of `tasks` """
        self.tasks.reverse()

    def index(self, task: Task) -> int | list[int]:
        """
        Return index of task in `tasks`

        If `task` is a string, search for string in tasks and return index of
        each result
        """
        if isinstance(task, str):
            search_results = self.find(task, as_project=False)
            return [self.index(t) for t in search_results]
        return self.tasks.index(task)

    def pop(self, task_num: int) -> Task:
        """ Remove and return task at a given index """
        task = self[task_num]
        self.remove(task)
        return task

    def remove(self, task: Task | Project | int) -> Project:
        """ Remove task from `tasks` """
        if isinstance(task, Project):
            for t in task.tasks:
                self.remove(t)
            return self

        if isinstance(task, int):
            task = self[task]
        for src_proj in self.sources(task):
            src_proj.remove(task)
        self.save_task_copy_to_history(task)
        self.tasks.remove(task)
        return self

    def _share_dark_setting(self, others):
        for other in others:
            other.dark = self.dark
        return others

    def sources(self, task: Task | int) -> list[Project]:
        """ Subproject that is the immediate parent of a particular Task """
        if isinstance(task, int):
            return self.sources(self[task])
        return self._share_dark_setting(
            [sp for sp in self.subprojects if task in sp]
        )

    def source_path(self, task: Task | int) -> list[str]:
        """
        Return list of subproject names giving path through subprojects
        tree to task.
        """
        ret = [self.name]
        if not self._is_leaf(task):
            sp = self.sources(task)[0]
            ret += sp.source_path(task)  # recurse
        return ret

    def source_path_string(self, task: Task | int) -> str:
        return " ⇐" + " ⇐".join(self.source_path(task)[1:])

    def color_sequence(self) -> list:
        return (
            DARK_BACKGROUND_COLOR_SEQUENCE
            if self.dark
            else LIGHT_BACKGROUND_COLOR_SEQUENCE
        )

    @staticmethod
    def tree_color(txt: str) -> str:
        return faint_txt(color_func(color=NAME_COLOR)(txt))

    @staticmethod
    def name_color(txt: str) -> str:
        return color_func(color=NAME_COLOR, attrs=['bold'])(txt)

    @staticmethod
    def done_color(txt: str) -> str:
        return color_func(color=DONE_COLOR)(txt)

    def header(
        self,
        colors: bool | None = None,
        show_filename: bool = False
    ):
        if colors is None:
            colors = self.colors
        ret = ""
        if self.name not in ['', None]:
            name_str = self.name
            if colors:
                name_str = self.name_color(name_str)
            ret += name_str
        if len(self.notes) > 0:
            ret += "* "
            note = self.notes[0]
            note_text = note.text
            if len(note_text) > 0:
                max_show_note_len = self.get_width() - 8
                show_note_text = note_text.splitlines()[0]
                if len(show_note_text) > max_show_note_len:
                    show_note_text = show_note_text[:max_show_note_len] + "..."
                ret += "\n *" + show_note_text

        # show marker indicating hidden completed tasks, if appropriate
        if (
            not self.show_completed
            and len([t for t in self.tasks if t.status is True]) > 0
        ):
            ret += self.done_color("... ")

        if show_filename and self.filename not in ['', None]:
            ret += f"({self.filename})"
        ret += "\n"
        if self.description not in ['', None]:
            ret += f"{self.description}\n"
        if self.status not in ['', None]:
            ret += f"status: {self.status}\n"

        for _tag in self.tags:
            ret += f"  ^{_tag}"
        if len(self.tags) > 0:
            ret += "\n"

        for person in self.people:
            ret += f"  @{person}"
        if len(self.people) > 0:
            ret += "\n"

        return ret

    def display_task(
        self,
        task_number: int,
        task: Task,
        colors: bool | None = None,
        is_last_line: bool = False,
        line_spacing: int = 1
    ):
        if colors is None:
            colors = self.colors
        ret = ""
        for _ in range(1, line_spacing):
            spacer = "│"
            if colors:
                spacer = self.tree_color(spacer)
            spacer += "\n"
            ret += spacer
        if is_last_line:
            tree_chars = "└──"
            begin_line = ""
        else:
            tree_chars = "├──"
            begin_line = "│"
        prefix = f"{tree_chars}{task_number :>3}. "
        prefix_len = len(prefix)
        if colors:
            prefix = self.tree_color(prefix)
            begin_line = self.tree_color(begin_line)
        postscript = (
            self.source_path_string(task)
            if not self._is_leaf(task) and self.show_source
            else ""
        )

        ret += task.displayln(
            begin_first_line=prefix,
            begin_line=begin_line,
            colors=colors,
            end_line=postscript,
            dark=self.dark,
            n_spaces_indent=N_SPACES_INDENT + prefix_len,
            width=self.get_width() - prefix_len,
            show_assignments=self.show_assignments,
            show_tags=self.show_tags,
            show_notes_asterisk=self.show_notes_asterisk
        )
        return ret

    def display_notes(self):
        ret = ""
        if self.show_notes:
            for note in self.notes:
                ret += "\n* " + note.text
        return ret

    def display_tasks(
        self,
        line_spacing: int | None = None,
        colors: bool | None = None,
        begin: int = 0,
        end: int | None = None
    ):
        if colors is None:
            colors = self.colors
        if line_spacing is None:
            line_spacing = self.line_spacing
        tasks = self.tasks
        if not self.show_completed:
            tasks = [t for t in tasks if t.status is not True]
        if len(tasks) == 0:
            ret = ["(No tasks)\n"]
        else:
            ret = []
            if end is None:
                end = len(tasks)
            for tn, task in enumerate(tasks[begin: end + 1]):
                is_last_line = tn == len(tasks) - 1 and not task.expanded
                ret.append(
                    self.display_task(
                        tn, task, colors=colors, is_last_line=is_last_line,
                        line_spacing=line_spacing
                    )
                )
        return ret

    def __repr__(
        self,
        line_spacing: int | None = None,
        colors: bool | None = None,
        show_filename: bool = False
    ):
        if colors is None:
            colors = self.colors
        self.update()
        ret = self.header(colors=colors, show_filename=show_filename)
        ret += "".join(
            self.display_tasks(line_spacing=line_spacing, colors=colors)
        )
        ret += self.display_notes()
        return ret

    def subproject_names(self) -> list[str]:
        ret = []
        for sp in self.subprojects:
            ret.append(sp.name)
            ret += sp.subproject_names()
        return ret

    def subproject_names_dict(self) -> dict:
        return {
            _key: _val for (_key, _val) in enumerate(self.subproject_names())
        }

    def numbered_history(self) -> dict:
        return dict(enumerate(self.history))

    def print_numbered_history(self):
        print("\nhistory:")
        for (key, val) in self.numbered_history().items():
            print(f"{key}: {val}")

    def _is_leaf(self, task: Task | int) -> bool:
        """ Whether task belongs to this Project as opposed to a subproject """
        return len(self.sources(task)) == 0

    def leaf_tasks(self) -> list:
        """ Tasks belonging to this Project as opposed to any subproject """
        return list(filter(self._is_leaf, self.tasks))

    def get_tree(
        self,
        level: int = 0,
        show_filename: bool = False,
        show_tasks: bool = True,
        dark: bool | None = None
    ):
        """ Text representation of tree of subproject relationships """
        if dark is None:
            dark = self.dark
        tasks_tree_ordered = []
        ret_str = f"{self.name_color(self.name)}"
        if show_filename:
            ret_str += f"({self.filename})"
        ret_str += "\n"
        if show_tasks:
            lvs = self.leaf_tasks()
            for tn, t in enumerate(lvs):
                ret_str += self.tree_color(
                    "└"
                    if (tn == len(lvs) - 1 and len(self.subprojects) == 0)
                    else "├"
                )
                ret_str += self.tree_color("─")
                ret_str += t.displayln(dark=dark)
                tasks_tree_ordered.append(t)
        for sp in self.subprojects:
            ts_lines, more_tasks = sp.get_tree(
                level=level + 1,
                show_filename=show_filename,
                show_tasks=show_tasks,
                dark=dark
            )
            tasks_tree_ordered += more_tasks
            ts_lines = ts_lines.splitlines()
            ret_str += self.tree_color("└─") + ts_lines[0] + "\n"
            if len(ts_lines) > 1:
                ret_str += "\n".join(
                    [
                        "  " + line
                        for line in ts_lines[1:]
                    ]
                ) + "\n"  # recurse
        return ret_str, tasks_tree_ordered

    def tree_string(self, **kwargs) -> str:
        return self.get_tree(**kwargs)[0]

    def tree(self, **kwargs):
        """ Show subprojects tree """
        print(self.tree_string(**kwargs))

    @property
    @wraps(tree)
    def t(self):
        return self.tree()

    def _to_dict_without_tasks(self, subprojects: bool = True) -> dict:
        ret = {
            "name": self.name,
            "description": self.description,
            "status": self.status,
            "notes": self.notes,
            "tags": self.tags,
            "people": self.people
        }
        if self.sortedby is not None:
            ret["sortedby"] = self.sortedby
        if subprojects:
            ret["subprojects"] = [
                subproject.filename for subproject in self.subprojects
            ]

        # simplify output by deleting empty elements, which the Project
        # constructor will create automatically
        unneeded_keys = [
            _key for (_key, _val) in ret.items() if len(_val) == 0
        ]
        for _key in unneeded_keys:
            del ret[_key]
        return ret

    def to_dict(self) -> dict:
        ret = self._to_dict_without_tasks()
        ret["tasks"] = [task.to_dict() for task in self.leaf_tasks()]
        return ret

    def to_json(self, filename: str | None = None) -> str:
        """
        Format Project data as .json, and write to a file if a filename
        is provided.
        """
        to_dump = self.to_dict()
        if filename is None:
            return json.dumps(to_dump, indent=JSON_INDENT)
        else:
            with open(filename, 'w') as ff:
                json.dump(to_dump, ff, indent=JSON_INDENT)

    @property
    def filepath(self):
        return pathlib.Path(self.filename).expanduser()

    def save(
        self,
        filename: str | None = None,
        backup: bool = True,
        messages: bool = True,
        depth: int = 0,
        max_depth: int | None = None,
        file_type: str | None = None,
    ):
        """
        Save Project to .json or .txt file,
        and recursively save its subprojects
        """
        if filename is None:
            filename = self.filename
        else:
            self.filename = filename

        if file_type is None:
            file_type = self.filepath.suffix

        msg = ""
        if backup and pathlib.Path(self.filename).exists():
            # fn = self.filename.replace(' ', '\\ ')
            fn = self.filename
            shutil.copyfile(fn, fn + "~")
            if messages:
                msg += "  ~ ✓ "

        filename = str(pathlib.Path(filename).expanduser())
        filename = "".join(filename.split(".")[:-1]) + file_type

        if file_type == ".json":
            self.to_json(filename=filename)
        else:
            self.to_todotxt(filename=filename)

        msg = f"Saved {self.name_color(self.name)} to {filename}" + msg
        if depth > 0:
            # indent for each level of subproject tree
            msg = "   " * (depth-1) + "└──" + msg
        if messages:
            print(msg)

        if max_depth is not None:
            if depth >= max_depth:
                return

        # recursively save subprojects
        for sp in self.subprojects:
            sp.save(
                backup=backup,
                messages=messages,
                depth=depth + 1,
                max_depth=max_depth,
                file_type=file_type
            )

    @property
    @wraps(save)
    def s(self):
        return self.save()

    def _undo(self) -> Project:
        """ Undo most recent operation in interactive mode """
        if len(self._undo_buffer) > 0:
            self._redo_buffer.append(self.copy())
            self.copy_from(self._undo_buffer.pop())
        return self

    def _redo(self) -> Project:
        """ Redo most recent undone operation in interactive mode """
        if len(self._redo_buffer) > 0:
            self._undo_buffer.append(self.copy())
            self.copy_from(self._redo_buffer.pop())
        return self

    def _get_first_td_file_in_folder(
        self, dirpath: str, default_file_name: str = "tada"
    ) -> str:
        """ Return first file in directory with default filename """
        files = pathlib.os.listdir(dirpath)
        for f in files:
            if (
                f.startswith(default_file_name)
                and f.endswith(self.default_file_type)
            ):
                return str(pathlib.Path(dirpath).joinpath(f))
        raise FileNotFoundError(
            f"path \'{dirpath}\' contains no files named "
            f"\'{default_file_name}*{self.default_file_type}\'"
        )

    @classmethod
    def _from_json(cls, filename: str) -> Project:
        """ Return Project reinstantiated from .json file """
        filename_path = pathlib.Path(filename)
        if filename_path.exists():
            if filename_path.is_dir():
                # if user provided a directory, look for first file
                # in directory named td*.json
                filename = cls._get_first_td_file_in_folder(filename)
            with open(filename, 'r') as ff:
                try:
                    pdict = json.load(ff)
                except TimeoutError as err:
                    print(filename)
                    raise err
            task_dicts = pdict.pop('tasks')
            tasks = [Task(**tdict) for tdict in task_dicts]
            return cls(tasks=tasks, filename=filename, **pdict)
        else:
            raise FileNotFoundError(f"{filename} not found")

    @classmethod
    def _from_todotxt(cls, filename: str) -> Project:
        """ Return Project reinstantiated from .txt file """
        filename_path = pathlib.Path(filename)
        if filename_path.is_file():
            try:
                ret = cls().from_todotxt(str(filename_path))
            except TimeoutError as err:
                print(filename)
                raise err
            else:
                return ret
        else:
            raise FileNotFoundError(f"{filename} not found")

    @classmethod
    def load(
        cls,
        filename: str | pathlib.Path,
        create_if_not_found: bool = False,
        dark: bool = False,
        sorted: bool = True
    ) -> Project:
        """ Reinstantiate Project from .json or .txt file """
        filepath = pathlib.Path(filename)
        suffix = filepath.suffix
        if suffix == ".json":
            func = cls._from_json
        else:
            func = cls._from_todotxt  # default to txt import
        try:
            ret = func(filename=filename)
        except (FileNotFoundError, json.decoder.JSONDecodeError) as _err:
            if create_if_not_found:
                return cls(filename=filename, dark=dark)
            else:
                print(f"{type(_err)}: {_err}")
                raise FileNotFoundError
        else:
            if sorted:
                ret.sort()
            return ret

    def sorted(self, by: str = 'urgency', reverse: bool = False) -> Project:
        """ Return a copy of this Project with Tasks sorted """
        if by is None or len(self.tasks) == 0:
            return self
        sorted_tasks = self.tasks

        def sort_by_duedate(_tasks, _reverse=reverse):
            return sorted(
                _tasks,
                key=lambda task: (
                    task.duedate.toordinal()
                    if isinstance(task.duedate, datetime.date)
                    else math.inf
                ),
                reverse=_reverse
            )

        if by in ["urgency", "priority"]:
            sorted_tasks = sort_by_duedate(self.tasks, _reverse=reverse)
            reverse = not reverse

        if isinstance(by, list):
            ret = self
            for _sortkey in by:
                ret = ret.sorted(by=_sortkey, reverse=reverse)
            return ret
        elif by == "urgency":
            sorted_tasks = sorted(
                sorted_tasks,
                key=lambda task: task.urgency(),
                reverse=reverse
            )
        elif by == "status":
            sorted_tasks = sorted(
                sorted_tasks,
                key=lambda task: (
                    int(task.status)
                    if isinstance(task.status, bool)
                    else 0.5
                ),
                reverse=reverse
            )
        elif by == "duedate":
            sorted_tasks = sort_by_duedate(self.tasks)
        elif hasattr(self.tasks[0], by):
            def _key(task):
                _ret = getattr(task, by)
                if isinstance(_ret, bool):
                    _ret = int(_ret)
                elif isinstance(_ret, datetime.date):
                    _ret = str(_ret)
                return _ret

            sorted_tasks = sorted(
                self.tasks, key=_key, reverse=reverse
            )
        else:
            print("Invalid sort _key. Options:")
            for _key in self.tasks[0].to_dict():
                print("  " + _key)
            raise KeyError

        sorted_project = self.copy()
        sorted_project.tasks = sorted_tasks
        # make sure same tasks are selected after reordering
        sorted_project.set_selected_tasks([
            sorted_project.index(self[item])
            for item in self.selected_task_nums
        ])
        sorted_project.sortedby = by
        return sorted_project

    # overloads of list methods

    def sort(self, **kwargs) -> Project:
        """ Sort this Project's Tasks in-place """
        sorted_project = self.sorted(**kwargs)
        self.tasks = sorted_project.tasks
        self.set_selected_tasks(sorted_project.selected_task_nums)
        self.sortedby = sorted_project.sortedby
        return self

    def __getitem__(
        self,
        task_num_or_proj: str | int | slice
    ) -> Task | Project:
        """
        Project[task_num] -> Task
        Project[subproject_name] -> subproject
        """
        if isinstance(task_num_or_proj, str):
            subproj_name = task_num_or_proj
            splt = subproj_name.split("/")
            if len(splt) > 1:
                subproj_name = splt[0]

            subproj = self._subproj_dict.get(subproj_name)
            if subproj is None:
                for sp in self.subprojects:
                    subproj = sp[subproj_name]  # recurse
                    if subproj is not None:
                        break

            # recurse
            if len(splt) > 1:
                rest_of_string = "/".join(splt[1:])
                subproj = subproj.__getitem__(rest_of_string)

            if subproj is None:
                print(
                    f"\'{task_num_or_proj}\' not found in subprojects tree"
                )
            else:
                return subproj

        elif isinstance(task_num_or_proj, (int, slice)):
            tn = task_num_or_proj
            t = self.tasks[tn]
            if isinstance(t, Task):
                t.dark = self.dark
            elif isinstance(t, (list, tuple)):
                for tt in t:
                    tt.dark = self.dark
            return t

    def __setitem__(self, _task_num: str | int | slice, task: Task | Project):
        """ Project[task_num] = Task """
        if isinstance(_task_num, str) and isinstance(task, Project):
            subproj_name = _task_num
            proj = task
            self._subproj_dict[subproj_name] = proj
            self.update()
        elif isinstance(_task_num, (int, slice)):
            self.tasks[_task_num] = task

    def __delitem__(self, _task_num: int):
        """ del Project[task_num] -> remove Task """
        if _task_num in range(len(self.tasks)):
            self.remove(self.tasks[_task_num])

    def __len__(self) -> int:
        """ number of tasks """
        return len(self.tasks)

    @classmethod
    def _to_project(
        cls,
        other: Project | str | Task | list | tuple,
        name: str = "new"
    ) -> Project:
        if isinstance(other, cls):
            # already a project, nothing to do
            return other
        if isinstance(other, str):
            other = [Task(other)]
        if isinstance(other, Task):
            other = [other]
        if isinstance(other, (list, tuple)):
            other = [
                Task(item) if isinstance(item, str) else item
                for item in other
            ]
        return cls(tasks=other, name=name)

    def __iadd__(self, other: Project | list | tuple | str | Task) -> Project:
        """
        Project += Task -> add Task to Project's tasks
        or project1 += project2 -> add project2 as subproject of project1
        """

        if isinstance(other, Project):
            self.add_subproject(other)
            return self

        if isinstance(other, (list, tuple)):
            for item in other:
                self.__iadd__(item)
            return self

        if isinstance(other, str):
            self.__iadd__(Task(other))
            return self

        if isinstance(other, Task):
            other.dark = self.dark
            self.tasks.append(other)
            return self

        raise TypeError(
            "__iadd__ requires str, Task, Project, or list of Tasks"
        )

    def __add__(
        self,
        other_project: Project | str | Task | list | tuple
    ) -> Project:
        """
        Project + Project -> new Project;
        or Project + Task -> add Task to copy of Project.
        """
        if other_project is self:
            raise ValueError("Cannot add a Project to itself.")
        other_project = Project._to_project(other_project)
        ret = Project(
            name=self.name + " + " + other_project.name,
            filename=TMP_FILENAME,
            dark=self.dark
        )
        ret += self
        ret += other_project
        return ret

    def _get_subproject(self, other: Project | str) -> Project | None:
        if other in self.subprojects:
            return other
        if other in self._subproj_dict:
            return self._subproj_dict[other]
        print(
            f"Cannot find \'{other}\' at top level of subproject tree. "
            f"Please enter a Project or the name of a top-level Project "
            f"from among: \n"
            + str(self.top_level_subproject_names())
        )
        return None

    def __isub__(self, other: str | Task | Project) -> Project:
        """ remove Task or subproject from Project """
        if isinstance(other, str):
            other = self._get_subproject(other)
        if isinstance(other, Task):
            self.remove(other)
        elif isinstance(other, Project):
            self.remove_subproject(other)
        else:
            raise TypeError(
                "Project.__isub__ requires a Task, subproject, or name of "
                "subproject"
            )
        return self

    def __sub__(self, other: str | Task | Project) -> Project:
        """ copy of Project with Task or subproject removed """
        ret = self.copy()
        if isinstance(other, (str, Project)):
            ret.name = self.name + " - " + str(other)
        ret.filename = TMP_FILENAME
        try:
            ret -= other
        except TypeError:
            raise TypeError(
                "Project.__sub__ requires a Task, subproject, or name of "
                "subproject"
            )
        return ret

    def __contains__(self, task_or_proj: Task | Project) -> bool:
        """
        Task in Project -> boolean
        or: Subproject in Project -> boolean
        """
        if isinstance(task_or_proj, Task):
            return task_or_proj in self.tasks
        elif task_or_proj in self.subprojects:
            return True
        else:
            return any(task_or_proj in sp for sp in self.subprojects)

    def __str__(self) -> str:
        return self.name

    def __imul__(self, txt: str) -> Project:
        self.notes.append(Note(txt))
        return self

    def __mul__(self, txt: str) -> Project:
        """
        Project * txt -> copy of Project with txt concatenated onto notes
        """
        ret = self.copy()
        ret.__imul__(txt)
        return ret

    def __ne__(self, _priority: int) -> Project:
        for task in self.tasks:
            task.set_priority(_priority)
        return self

    def __imatmul__(self, person: str) -> Project:
        """ Assign all tasks to `person` """
        for task in self.tasks:
            task @= person
        return self

    def __ixor__(self, _tag: str) -> Project:
        """ Tag all tasks with `_tag` """
        for task in self.tasks:
            task ^= _tag
        return self

    def claim(self, subproject: Project | str) -> Project:
        """
        Remove subproject but keep its Tasks, so these become
        leaf-tasks of this Project. Use with caution!
        """
        subproject = self._get_subproject(subproject)
        if subproject is not None:
            self.subprojects.remove(subproject)
        return self

    def __iand__(self, other: list | tuple | Project | str | Task) -> Project:
        """
            project1 &= task1 -> add task1 to project1's tasks as leaf-task
            project1 &= project2 -> absorb project2's Tasks into project1
        """
        if isinstance(other, (list, tuple)):
            for item in other:
                self.__iand__(item)
                return self
        if isinstance(other, Project):
            self.__iand__(other.tasks)
            return self
        if isinstance(other, str):
            self.__iand__(Task(other))
            return self

        if isinstance(other, Task):
            self.append(other.copy())
            return self

        raise TypeError(
            "__iand__ requires str, Task, Project, or list of Tasks"
        )

    def __and__(self, other: list | tuple | Project | str | Task) -> Project:
        """
            project1 & task1 -> copy of project 1 with task1 added as leaf-task
            project1 & project2 -> copy of project1 with project2's Tasks absorbed
        """
        try:
            return self.copy().__iand__(other)
        except TypeError:
            raise TypeError(
                "__and__ requires str, Task, Project, or list of Tasks"
            )

    def indep(self) -> Project:
        """
        Return a copy of the Project with all Tasks claimed as direct
        members (no subprojects).
        """
        ret = self.copy()
        for subproject in ret.subprojects:
            ret.claim(subproject)
        return ret

    def top_level_subproject_names(self) -> list:
        return list(self._subproj_dict.keys())

    def copy(self) -> Project:
        """ Return a copy of this Project """
        ret = Project(**self._to_dict_without_tasks(subprojects=False))
        for task in self.leaf_tasks():
            ret &= task.copy()
        for subproject in self.subprojects:
            ret += subproject.copy()
        return ret

    def copy_from(self, other_project: Project) -> Project:
        """ Copy metadata and tasks from another Project """
        self.name = other_project.name
        self.notes = other_project.notes
        self.description = other_project.description
        self.status = other_project.status
        self.tags = other_project.tags.copy()
        self.people = other_project.people.copy()

        self.tasks = []
        self.subprojects = []
        for task in other_project.tasks:
            self.__iand__(task.copy())
        for subproject in other_project.subprojects:
            self.__iadd__(subproject.copy())
        return self

    def copy_task(self, task: Task) -> Task:
        ret = task.copy()
        ret.dark = self.dark
        return ret

    def save_task_copy_to_history(self, task: Task | None = None):
        if task is None:
            task = self.selected_task()
        self.history.append(self.copy_task(task))
        num_history_items_to_drop = len(self.history) - self.history_length_max
        if num_history_items_to_drop > 0:
            self.history = self.history[num_history_items_to_drop:]

    def _has_subproject(self, subproj: Project) -> bool:
        if self is subproj:
            return True
        for sp in self.subprojects:
            if subproj in sp.subprojects:
                return True
        return False

    def _has_subproject_overlap(self, other: Project) -> bool:
        return (
            self is other
            or self in other
            or other in self
        )

    def add_subproject(
        self, subproj: Project | str, filename: str | None = None
    ) -> Project:
        """ Make an existing Project a subproject of this Project """
        if isinstance(subproj, str):
            if filename is None:
                filepath = pathlib.Path(self.filename)
                filename = str(
                    filepath.with_name(f"{subproj}{self.default_file_type}")
                )
            subproj = Project(name=subproj, filename=filename)
        if subproj.name in self.top_level_subproject_names():
            raise ValueError(
                f"Sorry, a project of name \'{str(subproj)}\' is already"
                f" a subproject at top level."
            )
        elif self._has_subproject_overlap(subproj):
            raise ValueError(
                f"Sorry, can't add Project \'{subproj.name}\' as a subproject "
                f"of \'{self.name}\' as this would create loops "
                "in the subproject tree."
            )
        else:
            subproj.dark = self.dark
            self.subprojects.append(subproj)
            self.tasks += subproj.tasks
        return self

    def _move_task_to_subproject(
        self, subproj: Project | str, task: Task
    ) -> Project:
        subproj = self._get_subproject(subproj)
        assert subproj in self
        task_number = self.index(task)
        self.__isub__(task)
        subproj &= task
        self.tasks.insert(task_number, task)
        self.update()
        return subproj

    def add_to_project(self, superproj: Project):
        """ Make this Project a subproject of another existing Project """
        superproj.add_subproject(self)
        superproj.save()

    def add_to_umbrella(self):
        umbrella = Project.get_umbrella()
        self.add_to_project(umbrella)

    @wraps(dict.get)
    def get(self, txt):
        return self._subproj_dict.get(txt)

    def tag(self, new_tags: list[str] | tuple[str] | str):
        for task in self.tasks:
            task.tag(new_tags)
        return self

    def untag(self, _tag: str | None = None) -> Project:
        for task in self.tasks:
            task.untag(_tag)
        return self

    def __xor__(self, _tag: str | list[str] | tuple[str]) -> Project:
        return self.filter(tags=_tag)

    def assign(self, _people: str | list[str] | tuple[str]) -> Project:
        for task in self.tasks:
            task.assign(_people)
        return self

    def unassign(self, person: str | None = None) -> Project:
        for task in self.tasks:
            task.unassign(person)
        return self

    def __matmul__(self, _person: str | list[str] | tuple[str]) -> Project:
        return self.filter(people=_person)

    def remove_subproject(
        self,
        subproj: Project | str,
        keep_tasks: bool = False
    ):
        subproj = self._get_subproject(subproj)
        if subproj is not None:
            to_delete = [
                task for task in self.tasks
                if subproj in self.sources(task)
            ]
            if not keep_tasks:
                for task in to_delete:
                    idx = self.tasks.index(task)
                    del self.tasks[idx]
            sp_idx = self.subprojects.index(subproj)
            del self.subprojects[sp_idx]

    def update(self):
        """ Update Project after change to subproject """
        for sp in self.subprojects:
            sp.update()  # recurse
            for t in sp.tasks:
                if t not in self.tasks:
                    self.__iadd__(t)
        for tn, task in enumerate(self.tasks):
            task.selected = (tn in self.selected_task_nums)

    def _shift_selection(self, shift: int):
        self.selected_task_nums[0] = (
            (self.selected_task_nums[0] + shift) % len(self.tasks)
        )
        self.set_selected_tasks()

    def _select_next(self):
        """ Change selection to next Task """
        self._shift_selection(1)

    def _select_prev(self):
        """ Change selection to previous Task """
        self._shift_selection(-1)

    def set_selected_tasks(
        self,
        task_nums: int | list[int] | tuple[int] | None = None
    ):
        if task_nums is not None:
            if isinstance(task_nums, int):
                task_nums = [task_nums]
            self.selected_task_nums = task_nums
        for n, t in enumerate(self.tasks):
            t.selected = (n in self.selected_task_nums)
            if not t.selected and t.select_field_mode:
                t.toggle_select_field_mode()

    def selected_task(self, save_to_history: bool = False):
        """ Task(s) currently selected """
        if len(self.selected_task_nums) > 0:
            # if we have no tasks yet, create one blank task
            if len(self.tasks) == 0:
                self.tasks.append(Task())
            tn = self.selected_task_nums[0]
            tn = min(tn, len(self.tasks) - 1)
            tn = max(tn, 0)
            ret = self[tn]
            if save_to_history:
                self.save_task_copy_to_history(ret)
            return ret
        else:
            return None

    def find(self, txt: str, as_project: bool = True) -> list[Task] | Project:
        """ Search for tasks containing text """
        def task_has_txt(task, text):
            return any([
                text in getattr(task, task_attr_name)
                for task_attr_name in ["description", "people", "tags"]
            ])

        search_result = [t for t in self.tasks if task_has_txt(t, txt)]
        if as_project:
            return Project(
                name=f'"{txt}" in {self.name}',
                tasks=search_result,
                filename=None,
                dark=self.dark
            )
        else:
            return search_result

    def __mod__(self, txt: str) -> Project:
        return self.find(txt)

    def _enter_interactive_task_edit(self, post: str = ""):
        t = self.selected_task()
        self.save_task_copy_to_history(t)
        t.toggle_select_field_mode()
        self._redisplay(post=post)

    def _redisplay(self, post: str = "", clear: bool = False):
        header = self.header()
        n_lines_header = len(header.splitlines())
        line_indices = [0]
        all_task_lines = self.display_tasks()
        for task_lines in all_task_lines:
            line_indices.append(line_indices[-1] + len(task_lines.splitlines()))
        line_indices = line_indices[1:]
        n_lines_post = len(post.splitlines()) + 2
        max_task_lines = (
            self.terminal.height - n_lines_header - n_lines_post
            - self.line_spacing
        )
        begin_task_idx = 0
        begin_line_idx = 0
        if len(self.selected_task_nums) > 0:
            selected_task_num = self.selected_task_nums[0]
            selected_task_line_idx = (
                line_indices[selected_task_num]
                if selected_task_num < len(line_indices)
                else sum(
                    len(task_lines.splitlines())
                    for task_lines in all_task_lines
                )
            )
            if selected_task_line_idx > max_task_lines:
                begin_line_idx = (
                    selected_task_line_idx - max_task_lines + self.line_spacing
                )
                while line_indices[begin_task_idx] <= begin_line_idx:
                    begin_task_idx += 1
        if len(self.selected_task_nums) > 0:
            end_task_idx = self.selected_task_nums[0]
        else:
            end_task_idx = begin_task_idx
        end_line_idx = begin_line_idx + max_task_lines
        while True:
            if end_task_idx >= len(line_indices):
                break
            if line_indices[end_task_idx] >= end_line_idx:
                break
            end_task_idx += 1

        self.update()
        to_print = header
        for task_lines in all_task_lines[begin_task_idx:end_task_idx + 1]:
            to_print += task_lines
        to_print += "\n"
        to_print += post
        hide_cursor_ansi_code = "\x1b[?25l"
        show_cursor_ansi_code = "\x1b[?25h"
        if clear:
            subprocess.run("clear" if os.name == "posix" else "cls")
        print(
            self.terminal.home,
            self.terminal.clear_eos(),
            hide_cursor_ansi_code,
            to_print,
            show_cursor_ansi_code,
            end="", sep=""
        )

    def _get_selected_task_num(self):
        if len(self.selected_task_nums) == 0:
            self.selected_task_nums = [0]
        return self.selected_task_nums[0]

    def _keypress_handler(self, _key: str):
        stop_listening()
        task = self.selected_task()
        cycle_through_notes = (
            task.select_field_mode
            and 4 in task.selected_fields
            and len(task.notes) > 1
        )
        page_up_down_shift = 5
        if _key == "tab":
            if task.expanded:
                _key = "right"
            else:
                _key = "down"
        if _key == 'down':
            if cycle_through_notes:
                task.select_next_note()
            else:
                self._select_next()
        elif _key == 'up':
            if cycle_through_notes:
                task.select_prev_note()
            else:
                self._select_prev()
        elif _key == "pageup":
            if cycle_through_notes:
                task.select_prev_note()
            else:
                task_num = self._get_selected_task_num()
                if task_num == 0:
                    self._shift_selection(-1)
                elif 0 < task_num < page_up_down_shift:
                    self.set_selected_tasks([0])
                else:
                    self._shift_selection(-page_up_down_shift)
        elif _key == "pagedown":
            if cycle_through_notes:
                task.select_next_note()
            else:
                task_num = self._get_selected_task_num()
                if task_num == len(self.tasks) - 1:
                    self._shift_selection(1)
                elif (len(self.tasks) - page_up_down_shift < task_num):
                    self.set_selected_tasks([len(self.tasks) - 1])
                else:
                    self._shift_selection(page_up_down_shift)
        elif _key == "right":
            if task.select_field_mode:
                task.next_field()
            else:
                self.cmd_list.append("edit task")
        elif _key == "left":
            if task.select_field_mode:
                task.prev_field()
            else:
                task.selected_fields = [0]
                task.prev_field()
                self.cmd_list.append("edit task")
        elif _key == 'z':
            self._undo()
        elif _key == 'y':
            self._redo()
        elif _key == "s":
            self.save(messages=True)
            sleep(MESSAGE_SLEEP_TIME)
        elif _key == "w":
            self.cmd_list.append("save as")
        elif _key == "q":
            self.cmd_list.append("break")
        elif _key == "esc":
            if task.select_field_mode:
                task.toggle_select_field_mode()
            else:
                self.cmd_list.append("break")
        elif _key == "?":
            if "help" in self.cmd_list:
                self.cmd_list.remove("help")
            else:
                self.cmd_list.append("help")
        elif _key == "enter":
            if task.select_field_mode:
                self.cmd_list.append("edit field")
            else:
                self.cmd_list.append("edit task")
        elif _key == "space":
            self.cmd_list.append("toggle expanded")
        else:
            self._undo_buffer.append(self.copy())
            self.cmd_list.append(_key)

    def move_task_up(self, tn: int):
        """ Move Task one step up in Project """
        if tn > 0:
            tmp = self[tn - 1]
            self[tn - 1] = self[tn]
            self[tn] = tmp
            self._select_prev()

    def move_task_down(self, tn: int):
        """ Move Task one step down in Project """
        if tn < len(self.tasks) - 1:
            tmp = self[tn + 1]
            self[tn + 1] = self[tn]
            self[tn] = tmp
            self._select_next()

    def move_task_top(self, tn: int):
        """ Move Task to top of Project """
        while tn > 0:
            self.move_task_up(tn)
            tn -= 1

    def move_task_bottom(self, tn: int):
        """ Move Task to bottom of Project """
        while tn < len(self.tasks) - 1:
            self.move_task_down(tn)
            tn += 1

    def _interactive_sort(self, reverse: bool = False):
        selected_task = self.selected_task()
        if selected_task is not None:
            if selected_task.select_field_mode:
                selected_field_nums = selected_task.selected_fields
                if len(selected_field_nums) > 0:
                    sort_key = (
                       selected_task.field_names[selected_field_nums[0]]
                    )
                    self.sort(by=sort_key, reverse=reverse)
                    return

        sort_keys = [
            'urgency',
            'status',
            'priority',
            'duedate',
            'description',
            'tags',
            'people'
        ]
        print(
            'sort by: '
            + ", ".join([
                '[' + str(n) + '] ' + sk
                for n, sk in enumerate(sort_keys)
            ])
            + '; append [-] to reverse')
        try:
            sort_key = input("  : ").strip()
        except KeyboardInterrupt:
            return

        def _on_invalid_sort_key():
            print("Invalid sort _key.")
            sleep(MESSAGE_SLEEP_TIME)

        if len(sort_key) > 0:
            if sort_key[-1] == '-':
                sort_key = sort_key[:-1]
                reverse = not reverse
            if sort_key.isnumeric():
                try:
                    sort_key = sort_keys[int(eval(sort_key))]
                except IndexError:
                    _on_invalid_sort_key()
                    return
            if sort_key in sort_keys:
                self.sort(by=sort_key, reverse=reverse)
            else:
                _on_invalid_sort_key()
        else:
            self.sort(reverse=reverse)

    async def _do_listen_keyboard(self):
        try:
            listen_keyboard(
                on_press=self._keypress_handler,
                sequential=True,
                delay_second_char=0.1,
                delay_other_chars=0.1,
                until=None
            )
        except KeyboardInterrupt:
            self._keypress_handler("esc")

    def generate_hints_bar(self, column_pad=COLUMN_PAD):
        ret = ""
        hint_strings = [f"{key}: {val}" for (key, val) in self.hints.items()]
        hints_column_width = max(
            HINTS_COLUMN_WIDTH,
            max(len(s) for s in hint_strings)
        )
        num_hints_columns = int(self.get_width() / hints_column_width)
        col_width = max(
            hints_column_width,
            max([len(s) for s in hint_strings]) + column_pad
        )
        formatted_hint_strings = [
            f"{key}" + faint_txt(f" {val}")
            for (key, val) in self.hints.items()
        ]
        for i, (hint_string, formatted_hint_string) in enumerate(
            zip(hint_strings, formatted_hint_strings)
        ):
            if i % num_hints_columns == num_hints_columns - 1:
                sep = "\n"
            else:
                sep = " " * (col_width - len(hint_string))
            ret += f"{formatted_hint_string}{sep}"
        ret += "\n"
        return ret

    def _interactive_assign(
        self, task: Task | None = None, append: bool = False
    ):
        if task is None:
            task = self.selected_task()
        old_people_string = " ".join(["@" + person for person in task.people])
        print("\nAssign to" + ("" if append else " (overwrite)") + ":")
        _people_string = clipboard_input(old_text=old_people_string)
        if _people_string is None:
            return
        _people = [
            person.strip()
            for person in _people_string.split("@")
            if len(person) > 0
        ]
        if append:
            task.people += _people
        else:
            task.people = _people
        return task

    def _interactive_tag(self, task: Task | None = None, append: bool = False):
        if task is None:
            task = self.selected_task()
        old_tags_string = " ".join(
            ["#" + tag for tag in task.tags]
        )
        print("\nTags: ")
        _tags_string = clipboard_input(old_text=old_tags_string)
        if _tags_string is None:
            return
        _tags = [
            tag.strip()
            for tag in _tags_string.split("#")
            if len(tag) > 0
        ]
        if append:
            task.tags += _tags
        else:
            task.tags = _tags
        return task

    async def _display_loop(
        self,
        post: str = (
            "?: keyboard shortcuts\tq: exit interactive mode\n"
        )
    ):
        if self.colors:
            post = faint_txt(post)
        post_now = post
        while True:
            if len(self.selected_task_nums) == 0:
                self.selected_task_nums = [0]
            if self.selected_task_nums[0] >= len(self.tasks):
                self.selected_task_nums[0] = len(self.tasks) - 1
            tn = self.selected_task_nums[0]
            self._redisplay(post=post_now)
            await self._do_listen_keyboard()
            if "help" in self.cmd_list:
                self.hints_bar = self.generate_hints_bar()
                post_now = self.hints_bar
            else:
                post_now = post

            for cmd in self.cmd_list:
                if cmd != "help":
                    self.cmd_list.remove(cmd)
                if cmd == "break":
                    self.selected_task().toggle_select_field_mode(False)
                    self.selected_task_nums = []
                    self._redisplay(post="")
                    return False
                if cmd == "exit":
                    sys.exit()
                elif cmd == "save as":
                    try:
                        print("Save as:")
                        filename = clipboard_input(old_text=self.filename)
                    except KeyboardInterrupt:
                        continue
                    else:
                        if filename is None:
                            continue
                        if "." not in filename:
                            filename += self.default_file_type
                        self.filename = filename
                        self.save(backup=False)
                        print("saved to " + self.filename)
                        sleep(MESSAGE_SLEEP_TIME)
                elif cmd == "toggle expanded":
                    self.selected_task().toggle_expanded()
                elif cmd == "x":
                    self.selected_task(save_to_history=True).x()
                elif cmd == "o":
                    self.selected_task(save_to_history=True).o()
                elif cmd == "/":
                    self.selected_task(save_to_history=True).set_status("/")
                elif cmd == "[":
                    self.move_task_up(tn)
                elif cmd == "]":
                    self.move_task_down(tn)
                elif cmd == "{":
                    self.move_task_top(tn)
                elif cmd == "}":
                    self.move_task_bottom(tn)
                elif cmd == "<":
                    self.selected_task(save_to_history=True).decrement_duedate()
                elif cmd == ">":
                    self.selected_task(save_to_history=True).increment_duedate()
                elif cmd == ".":
                    self.selected_task(save_to_history=True).set_duedate(None)
                elif cmd == "%":
                    self.selected_task().selected_fields = [2]
                    task = self.selected_task(save_to_history=True)
                    task._interactive_edit_field("duedate")
                elif cmd == "!":
                    task = self.selected_task(save_to_history=True)
                    task.increment_priority()
                elif cmd == "~":
                    task = self.selected_task(save_to_history=True)
                    task.decrement_priority()
                elif cmd == "g":
                    task = self.selected_task(save_to_history=True)
                    try:
                        self.remove_tag_menu(task)
                    except KeyboardInterrupt:
                        continue
                elif cmd == "#":
                    task = self.selected_task(save_to_history=True)
                    try:
                        self.add_tag_menu(task)
                    except KeyboardInterrupt:
                        continue
                elif cmd == "a":
                    task = self.selected_task(save_to_history=True)

                    try:
                        self.unassign_task_menu(task)
                    except KeyboardInterrupt:
                        continue
                elif cmd == "@":
                    try:
                        self.assign_task_menu(
                            task=self.selected_task(save_to_history=True)
                        )
                    except KeyboardInterrupt:
                        continue
                elif cmd == "k":
                    print(self.terminal.clear)
                    self.prcalendar()
                    try:
                        input("\nPress [Enter] to return to interactive\n")
                        continue
                    except KeyboardInterrupt:
                        continue

                elif cmd == "+":
                    self.__iadd__([Task()])
                    # select new task
                    self.set_selected_tasks([len(self.tasks)])
                    t = self.selected_task()
                    # enter edit mode for description
                    t.toggle_select_field_mode()
                    t.selected_fields = [t.field_names.index("description")]
                    self._redisplay()
                    t.edit_selected_field()
                elif cmd == "-":
                    self.save_task_copy_to_history(self[tn])
                    del self[tn]
                elif cmd == "edit task":
                    self._enter_interactive_task_edit(post=post_now)
                elif cmd == "edit field":
                    self._redisplay(post="")
                    task = self.selected_task(save_to_history=True)
                    task.edit_selected_field()
                elif cmd == ":":
                    self._interactive_sort()
                elif cmd == ";":
                    self.sort()
                elif cmd == "*":
                    self.selected_task(save_to_history=True).edit_new_note()
                elif cmd == "h":
                    self.show_completed = not self.show_completed
                    self._redisplay()
                elif cmd == "c":
                    self.toggle_compact()
                    self._redisplay()
                elif cmd == "&":
                    self.move_task_to_subproject_menu(self.selected_task())
                elif cmd == "^":
                    self.add_new_subproject_interactive()
                elif cmd == "d":
                    self.toggle_show_details()
                elif cmd == "t":
                    print(self.terminal.clear())
                    self.tree()
                    try:
                        input("Press [Enter] to return to interactive\n")
                        continue
                    except KeyboardInterrupt:
                        continue

                elif cmd == "f":
                    self.interactive_find()
                elif cmd == "$":
                    self.set_selected_tasks(len(self.tasks) - 1)
                elif cmd == "n":
                    try:
                        task_num = input(self.prompt_style("\nJump to task number: "))
                    except KeyboardInterrupt:
                        continue
                    if task_num.isnumeric():
                        task_num = int(task_num)
                        self.set_selected_tasks(task_num)
                elif cmd == "b":
                    sources = self.sources(self.selected_task())
                    if len(sources) > 0:
                        first_source = sources[0]
                        await first_source.interactive()
                        self.update()
                # elif cmd == "v":
                #     self.print_numbered_history()
                #     try:
                #         input("\nPress [Enter] to return to interactive\n")
                #         continue
                #     except KeyboardInterrupt:
                #         continue

                elif cmd.isnumeric():
                    self.set_selected_tasks(int(cmd))
                else:
                    if cmd != "help":
                        print(
                            f"`{cmd}` is not a valid command; "
                            f"press `?` for keyboard shortcuts."
                        )
                        try:
                            sleep(MESSAGE_SLEEP_TIME)
                        except KeyboardInterrupt:
                            continue

    def toggle_show_details(self):
        current_visibility = self.show_tags
        new_visibility = not current_visibility
        self.show_tags = new_visibility
        self.show_assignments = new_visibility
        self.show_notes_asterisk = new_visibility
        self.show_source = new_visibility
        return self

    def move_task_to_subproject_menu(self, task):
        """ Terminal menu to move task to existing subproject"""
        options = [
            SOURCE_SYMBOL + name for name in self.top_level_subproject_names()
        ]
        terminal_menu = TerminalMenu(
            options, title="Move Task to sub-Project:"
        )
        menu_entry_index = terminal_menu.show()
        if menu_entry_index is None:
            return
        subproj = self._get_subproject(options[menu_entry_index][1:])
        self._move_task_to_subproject(subproj, task)
        return subproj

    def current_people(self):
        return sorted(
            list(set([person for t in self.tasks for person in t.people]))
        )

    def current_tags(self):
        return sorted(
            list(set([tag for t in self.tasks for tag in t.tags]))
        )

    def assign_task_menu(self, task: Task):
        if not isinstance(task, Task):
            return
        current_people = self.current_people()
        if len(current_people) == 0:
            return self._interactive_assign(task, append=True)
        current_people = [
            person
            for person in current_people
            if person not in task.people
        ]
        options = ["@" + person for person in current_people]
        options.append("New...")
        new_assignment_option_number = len(options) - 1
        if len(task.people) > 0:
            options.append("Remove...")
            remove_assignment_option_number = len(options) - 1
        else:
            remove_assignment_option_number = -1
        terminal_menu = TerminalMenu(options, title="Assign Task to: ")
        menu_entry_index = terminal_menu.show()
        if menu_entry_index is None:
            return
        if menu_entry_index == new_assignment_option_number:
            return self._interactive_assign(task, append=True)
        if menu_entry_index == remove_assignment_option_number:
            return self.unassign_task_menu(task)
        task.assign(options[menu_entry_index][1:])
        return task

    def unassign_task_menu(self, task: Task):
        if not isinstance(task, Task):
            return
        current_people = task.people
        if len(current_people) == 0:
            return
        options = ["@" + person for person in current_people]
        terminal_menu = TerminalMenu(options, title="Remove Task assignment: ")
        menu_entry_index = terminal_menu.show()
        if menu_entry_index is None:
            return
        task.unassign(options[menu_entry_index][1:])
        return task

    def add_tag_menu(self, task: Task):
        if not isinstance(task, Task):
            return
        current_tags = self.current_tags()
        if len(current_tags) == 0:
            return self._interactive_tag(task, append=True)
        current_tags = [
            tag for tag in current_tags if tag not in task.tags
        ]
        options = ["#" + tag for tag in current_tags]
        options.append("New...")
        add_tag_option_number = len(options) - 1
        if len(task.tags) > 0:
            options.append("Remove...")
            remove_tag_option_number = len(options) - 1
        else:
            remove_tag_option_number = -1
        terminal_menu = TerminalMenu(options, title="Add tag: ")
        menu_entry_index = terminal_menu.show()
        if menu_entry_index is None:
            return
        if menu_entry_index == add_tag_option_number:
            return self._interactive_tag(task, append=True)
        if menu_entry_index == remove_tag_option_number:
            return self.remove_tag_menu(task)
        task.tag(options[menu_entry_index][1:])
        return task

    def remove_tag_menu(self, task: Task):
        if not isinstance(task, Task):
            return
        current_tags = task.tags
        if len(current_tags) == 0:
            return
        options = ["#" + tag for tag in current_tags]
        terminal_menu = TerminalMenu(options, title="Remove tag: ")
        menu_entry_index = terminal_menu.show()
        if menu_entry_index is None:
            return
        task.untag(options[menu_entry_index][1:])
        return task

    def interactive_find(self):
        print("")
        try:
            txt = clipboard_input(
                old_text="", old_text_label="", prompt="Search text: "
            )
        except KeyboardInterrupt:
            return
        if len(txt) == 0:
            return
        search_results = self.find(txt)
        num_search_results = len(search_results.tasks)
        if num_search_results == 0:
            print(
                f"No results found matching \"{txt}\" "
                f"in Project `{self.name}`"
            )
            sleep(MESSAGE_SLEEP_TIME)
            return
        elif num_search_results == 1:
            chosen_task = search_results.tasks[0]
        else:
            brief_descriptions = [t.description for t in search_results.tasks]
            max_brief_description_length = max(10, self.get_width() - 4)
            for i, bd in enumerate(brief_descriptions):
                if len(bd) > max_brief_description_length:
                    brief_descriptions[i] = (
                            bd[:max_brief_description_length] + "..."
                    )
            print("")
            terminal_menu = TerminalMenu(
                brief_descriptions,
                title=search_results.name
            )
            menu_entry_index = terminal_menu.show()
            if menu_entry_index is None:
                return
            chosen_task = search_results[menu_entry_index]
        chosen_task_index_in_self = self.tasks.index(chosen_task)
        self.set_selected_tasks(chosen_task_index_in_self)
        return self

    def toggle_compact(self):
        """Toggle between single- and double-spacing of tasks"""
        if self.line_spacing == 1:
            self.line_spacing = 2
        else:
            self.line_spacing = 1

    async def interactive(self):
        """ Enter interactive mode """
        self.cmd_list = []
        self.selected_task_nums = [0]
        if len(self.tasks) == 0:
            self.__iadd__(Task())
        for t in self.tasks:
            t.select_field_mode = False
        self.update()
        await self._display_loop()
        self.selected_task_nums = []

    @property
    def i(self):
        return asyncio.run(self.interactive())

    @property
    def sp(self) -> list[Project]:
        """ Return list of subprojects """
        return self.subprojects

    @property
    def _subproj_dict(self) -> dict:
        """ Return dictionary of subprojects """
        ret = {sp.name: sp for sp in self.subprojects}
        for sp in ret.values():
            sp.dark = self.dark
        return ret

    @staticmethod
    def _to_list(thing: None | tuple | list) -> list:
        if thing is None:
            return []
        if isinstance(thing, (list, tuple)):
            return list(thing)
        else:
            return [thing]

    def _set_dark(self, dark: bool = False):
        self.dark = dark
        return self

    def filter(self, **kwargs) -> Project:
        def filter_func(task, **_kwargs):
            _status = _kwargs.get("status")
            if _status not in [None, task.status]:
                return False
            _duedate = _kwargs.get("duedate")
            if _duedate not in [None, task.duedate]:
                return False
            _description = _kwargs.get("description")
            if _description not in [None, task.description]:
                return False
            _tags = self._to_list(_kwargs.get("tags"))
            _tags += self._to_list(_kwargs.get("tag"))
            if (
                len(_tags) > 0
                and not any(_tag in task.tags for _tag in _tags)
            ):
                return False
            _people = self._to_list(_kwargs.get("people"))
            if (
                len(_people) > 0
                and not any(person in task.people for person in _people)
            ):
                return False
            _priority = _kwargs.get("priority")
            if _priority not in [None, task.priority]:
                return False
            return True

        filtered_project = self.copy().indep()
        filtered_project.name += "_filtered"
        filtered_project.tasks = [
            task for task in filtered_project.tasks
            if filter_func(task, **kwargs)
        ]
        return filtered_project

    def x(self, tn: int) -> Project:
        """ Mark Task done and highlight it """
        self.tasks[tn].x()
        self.set_selected_tasks(tn)
        return self

    def o(self, tn: int) -> Project:
        """ Mark Task not done and highlight it """
        self.tasks[tn].o()
        self.set_selected_tasks(tn)
        return self

    def deselect(self) -> Project:
        self.set_selected_tasks([])
        return self

    def calendar(
        self,
        year: int | None = None,
        month: int | None = None
    ) -> str:
        today = datetime.datetime.today()
        if year is None:
            year = today.year
        if month is None:
            month = today.month
        calendar_str = calendar.month(year, month)
        calendar_spl = calendar_str.splitlines()
        calendar_head = "\n".join(calendar_spl[:2])
        calendar_body = "\n".join(calendar_spl[2:])
        day_colors = {}
        if year == today.year and month == today.month:
            day_str = str(today.day)
            day_colors[day_str] = self.name_color(day_str)
        for task in self.sorted(by="urgency", reverse=True).tasks:
            if task.status is not True:
                duedate = task.duedate
                if isinstance(duedate, datetime.date):
                    if duedate.year == year and duedate.month == month:
                        day = duedate.day
                        day_colors[str(day)] = task.task_color(
                            day, attrs=["bold"]
                        )
        for _key, _val in day_colors.items():
            calendar_body = calendar_body.replace(_key, _val, 1)
        calendar_str = calendar_head + "\n" + calendar_body
        return calendar_str

    def prcalendar(self, year: int = None, month: int = None):
        print(self.calendar(year=year, month=month))

    def __call__(self, n: int):
        return self.__getitem__(n)

    def get_task(self, t: Task | int | str) -> Task | list[Task] | None:
        if isinstance(t, Task):
            return t
        if isinstance(t, int):
            return self[t]
        if isinstance(t, str):
            search_results = self.find(t, as_project=False)
            if len(search_results) > 0:
                return search_results[0]
            else:
                return None

    def move(
        self,
        task: Task | int | str | None = None,
        subproj: None | int | str | Project = None
    ):
        if task is None:
            try:
                task = self.tasks[
                    int(
                        input(f"task number: (0-{len(self.tasks)-1}): ")
                    )
                ]
            except KeyboardInterrupt:
                return

        sp_names_dict = self.subproject_names_dict()
        if subproj is None:
            try:
                subproj = input(f"subproject {sp_names_dict}: ")
            except KeyboardInterrupt:
                return
            if subproj.isnumeric():
                subproj = int(subproj)
        if isinstance(subproj, int):
            subproj = sp_names_dict[subproj]
        if isinstance(subproj, str):
            subproj_name = subproj
            subproj = self.__getitem__(subproj_name)
            if subproj is None:
                print(
                    f"No subproject with name \'{subproj_name}\'; task move "
                    f"cancelled."
                )
                return
        task = self.get_task(task)
        self.remove(task)
        subproj.__iadd__(task)
        return

    @staticmethod
    def _replace_suffix(filepath: pathlib.Path, new_suffix: str):
        return filepath.with_name(
            "".join(filepath.name.split(".")[:-1]) + ".txt"
        )

    def to_todotxt(
        self, filename: str | None = None, save: bool = True,
        links: bool = True, store_name: bool = True,
        leaf_tasks_only: bool = True
    ):
        task_copies = []
        for task in self.tasks:
            if leaf_tasks_only and not self._is_leaf(task):
                continue
            t = task.copy()
            t.project_names = [proj.name for proj in self.sources(task)]
            task_copies.append(t)
        ret = ""
        if store_name:
            ret += "# " + self.name + "\n\n"
        ret += "\n".join([t.to_todotxt() for t in task_copies]) + "\n"
        if links:
            for subproj_name in self.top_level_subproject_names():
                subproj = self._get_subproject(subproj_name)
                ret += f"+ ({subproj.name})[{subproj.filename}]\n"
        if filename is None:
            filename = self.filename
        filepath = pathlib.Path(filename)
        if filepath.suffix != ".txt":
            filepath = filepath.with_name(
                "".join(filepath.name.split(".")[:-1]) + ".txt"
            )
        filename = str(filepath)
        if save:
            with open(filename, "w") as f:
                f.write(ret)
        return ret

    def from_todotxt(
        self, filename: str, clear: bool = False
    ):
        self.filename = filename
        if clear:
            self.tasks.clear()
        with open(filename, "r") as f:
            lines = f.readlines()
        if lines[0].startswith("#"):
            self.name = lines[0].strip("# \n")  # take name from top line
            lines = lines[1:]
        else:
            # take name from filename
            self.name = "".join(os.path.basename(filename).split(".")[:-1])
        for line in lines:
            if line.startswith("+ "):
                line = line.strip("+ \n")
                if not ("[" in line and "]" in line):
                    continue
                filename_start_idx = line.index("[") + 1
                filename_end_idx = line.index("]")
                if filename_end_idx <= filename_start_idx:
                    continue
                filename = line[filename_start_idx:filename_end_idx]
                filepath = pathlib.Path(filename).expanduser()
                if filepath.is_file():
                    subproj = Project._from_todotxt(str(filepath))
                    self.__iadd__(subproj)
                else:
                    raise FileNotFoundError(
                        f"Could not find a file at {filename}."
                    )
            elif line.isspace():
                continue
            else:
                self.__iadd__(Task().from_todotxt(line))
        return self
