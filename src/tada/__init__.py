from __future__ import annotations
import nest_asyncio
from IPython.utils.terminal import set_term_title

from .project import Project
from .task import Task
from .note import Note


set_term_title("tada")
nest_asyncio.apply()


def get_umbrella() -> Project:
    return Project.get_umbrella()

# TODO recurring tasks
