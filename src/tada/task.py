from __future__ import annotations
from functools import wraps
from textwrap import fill
import datetime

from .utilities import *
from .settings import *
from .note import Note


CAPITAL_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

class Task:
    """
    An individual to-do item.
    """
    def __init__(
        self,
        description: str = "",
        status: bool | str = False,
        tags: list[str] | None = None,
        duedate: str | datetime.date | None = None,
        people: list[str] | None = None,
        priority: int = 0,
        notes: None | str | list[dict] | tuple[dict] = None,
        date_created: datetime.date | None = None,
        date_completed: datetime.date | None = None,
        max_priority: int = 5,
        dark: bool = DARK_BACKGROUND,
        project_names: list[str] | None = None
    ):

        self.status = status
        self._duedate = date_obj_from_string(duedate)

        if tags is None:
            tags = []
        self.tags = tags.copy()

        if people is None:
            people = []
        self.people = people.copy()

        if notes is None:
            notes = []
        if isinstance(notes, str):
            notes = [Note(notes)]
        else:
            notes = [Note(**note_dict) for note_dict in notes]
        self._selected_note_num = 0

        self.notes = notes
        if len(self.notes) > 0:
            self.notes[self.selected_note_num].selected = True

        self.max_priority = max_priority
        self._priority = int(priority)

        self.txt_background = None
        self._description = description
        self.expanded = False
        self.selected = False
        self.selected_fields = []
        self.select_field_mode = False
        self.field_names = [
            "status",
            "priority",
            "duedate",
            "description",
            "notes"
        ]

        if date_created is None:
            date_created = datetime.datetime.today().date()
        if isinstance(date_created, str):
            date_created = date_obj_from_string(date_created)
        self.date_created = date_created

        self.date_completed = date_completed

        self.dark = dark

        if project_names is None:
            project_names = []
        self.project_names = project_names

    @wraps(Note.__getitem__)
    def __getitem__(self, item):
        return self.notes.__getitem__(item)

    @wraps(Note.__setitem__)
    def __setitem__(self, key, value):
        return self.notes.__setitem__(key, value)

    @property
    def selected_note_num(self) -> int:
        self._selected_note_num %= len(self.notes)
        return self._selected_note_num

    @selected_note_num.setter
    def selected_note_num(self, num: int):
        if not isinstance(num, int):
            raise TypeError("Error: selected_note_num must be an integer")
        self._selected_note_num = num % len(self.notes)

    @property
    def selected_field_max(self) -> int:
        return len(self.field_names)

    def _extended_description(
            self,
            end_line: str = "",
            show_assignments: bool = True,
            show_tags: bool = True,
            show_notes_asterisk: bool = True,
            colors: bool = True
    ) -> str:
        ret = (
            self.description
            if len(self.description) > 0
            else BLANK_DESCRIPTION_STR
        )
        if colors:
            def tags_style(txt): return faint_txt(italic_txt(txt))
        else:
            def tags_style(txt): return txt
        assignments_style = tags_style
        notes_asterisk_style = tags_style
        if show_tags:
            ret += tags_style(
                "".join([f" #{_tag}" for _tag in self.tags])
            )
        if show_assignments:
            ret += assignments_style(
                "".join([f" @{_person}" for _person in self.people])
            )
        ret += end_line

        if show_notes_asterisk and len(self.notes) > 0:
            ret += notes_asterisk_style(" *")
        return ret

    def toggle_select_field_mode(self, to_select_field_mode: bool | None = None):
        """
        In interactive mode, change between task selection and field
        selection within task
        """
        if to_select_field_mode is None:
            to_select_field_mode = not self.select_field_mode
        self.select_field_mode = to_select_field_mode
        if self.select_field_mode:
            if len(self.selected_fields) == 0:
                self.selected_fields = [0]
            self.expanded = True
        else:
            self.selected_fields = []
            if len(self.notes) == 0:
                self.expanded = False

    def _checkbox(self) -> str:
        if isinstance(self.status, bool):
            return f"[{DONE_SYMBOL}]" if self.status else f"[{NOT_DONE_SYMBOL}]"
        else:
            return "[/]"

    @staticmethod
    def _date_to_str(_date: datetime.date | None) -> str:
        if _date is None:
            return ""
        return str(_date)

    def to_dict(self) -> dict:
        ret = {
            "description": self.description,
            "tags": self.tags,
            "duedate": str(self.duedate),
            "date_created": self._date_to_str(self.date_created),
            "date_completed": self._date_to_str(self.date_completed),
            "people": self.people,
            "notes": [note.to_dict() for note in self.notes]
        }

        # simplify output by deleting empty elements, which the Task
        # constructor will create automatically
        unneeded_keys = [key for (key, val) in ret.items() if len(val) == 0]
        for key in unneeded_keys:
            del ret[key]

        if self.status:
            ret["status"] = self.status
        if self.priority > 0:
            ret["priority"] = self.priority

        return ret

    def copy(self) -> Task:
        return Task(**self.to_dict())

    def toggle_expanded(self):
        self.expanded = not self.expanded
        if self.select_field_mode and not self.expanded:
            self.select_field_mode = False

    def _priority_string(self) -> str:
        priority = self.priority
        if priority <= 3:
            return "!" * priority + " " * (3 - priority)
        else:
            return "!" + str(priority) + (" " if priority < 10 else "")

    def displayln(
        self,
        colors: bool = True,
        begin_first_line: str = '',
        begin_line: str = '',
        end_line: str = '',
        dark: bool | None = None,  # whether background is dark
        show_notes: bool | None = None,
        n_spaces_indent: int = N_SPACES_INDENT,  # number of characters before description
        width: int | None = None,
        show_assignments: bool = True,
        show_tags: bool = True,
        show_notes_asterisk: bool = True
    ) -> str:
        """ Display Task, either individually or as item in Project
        """

        if dark is None:
            dark = self.dark
        if width is None:
            width = Terminal().width

        if not self.selected and len(self.notes) == 0:
            self.expanded = False

        if show_notes is None:
            show_notes = self.expanded

        def no_style(x):
            return x

        def _selected_style(txt):
            return self.task_color(txt, attrs=['reverse'], dark=dark)

        if colors:
            def task_style(txt, attrs=None):
                return self.task_color(txt, attrs=attrs, dark=dark)

            def selected_style_here(txt):
                return _selected_style(txt)
        else:
            task_style = no_style
            selected_style_here = no_style
        unselected_task_style = task_style
        if self.selected and not self.select_field_mode:
            task_style = _selected_style
        checkbox = (self._checkbox())

        pry_str = (self._priority_string())

        blank_date_style = faint_txt if colors else no_style
        date_str = (
            str(self.duedate)
            if self.duedate not in ["", None]
            else blank_date_style(BLANK_DATE_STR)
        )
        if str(datetime.datetime.today().year) == date_str[:4]:
            date_str = " " * 5 + date_str[5:]

        date_str += " "
        rhs = self._extended_description(
            end_line=end_line,
            show_tags=show_tags,
            show_assignments=show_assignments,
            show_notes_asterisk=show_notes_asterisk,
            colors=colors
        )
        rhs_style = task_style

        checkbox_style, priority_style, date_style, description_style = (
            (
                selected_style_here
                if self.select_field_mode and i in self.selected_fields
                else task_style
            )
            for i in range(4)
        )
        if self.select_field_mode and 4 in self.selected_fields:
            show_notes = True
            self.expanded = True

        space = task_style(" ")
        first_three_fields = (
            checkbox_style(checkbox) + space
            + priority_style("{: >4}".format(pry_str)) + space
            + date_style("{: >10}".format(date_str)) + space
        )

        min_column_width = 5
        rhs_wrapped = fill(
            rhs,
            width=max(min_column_width, width),
            break_long_words=False,
            drop_whitespace=False,
            replace_whitespace=False,
            subsequent_indent=n_spaces_indent * " "
        )
        rhs_wrapped = description_style(str(rhs_wrapped))
        rhs_wrapped = first_three_fields + ("\n" + " " * n_spaces_indent).join(
            rhs_wrapped.split("\n")
        )
        ret = rhs_style(rhs_wrapped)

        def format_task_lines_for_project(txt, _style=no_style, top=True):
            ret_splt = txt.splitlines()
            _ret = ""
            for n, line in enumerate(ret_splt):
                if len(line.strip()) > 0:
                    if n > 0 or not top:
                        _ret += begin_line
                        line = line.rstrip()
                    _ret += _style(line) + "\n"
            return _ret

        ret = format_task_lines_for_project(ret, task_style)
        if len(end_line) > 0 and colors:
            ret = ret.replace(end_line, faint_txt(end_line))

        def how_to_show_note(_note):
            plaintext_note = _note.plaintext_md_formatted()
            if len(plaintext_note) == 0:
                plaintext_note = " "
            _ret = ""
            for line in plaintext_note.splitlines():
                _ret += (
                    fill(line, width=width, subsequent_indent=" " * 4) + "\n"
                )
            return _ret

        if show_notes:
            if len(self.notes) == 0:
                notes = [Note(selected=True)]
            else:
                notes = self.notes
            if all(not note.selected for note in notes):
                notes[self.selected_note_num].selected = True
            for note in notes:
                style = (
                    selected_style_here
                    if (
                        self.selected
                        and (
                                (
                                    note.selected
                                    and self.select_field_mode
                                    and 4 in self.selected_fields
                                )
                                or (
                                    not self.select_field_mode
                                )
                        )
                    )
                    else unselected_task_style
                )
                ret += "\n".join(
                    begin_line + style(line)
                    for line in how_to_show_note(note).splitlines()
                ) + "\n"
        ret = begin_first_line + ret
        return ret

    def __repr__(self) -> str:
        show_notes = len(self.notes) > 0
        return self.displayln(show_notes=show_notes)

    def __str__(self) -> str:
        return self.displayln(colors=False).strip()

    def duedate_to_added_priority(self) -> int:
        if not isinstance(self.duedate, datetime.date):
            return 0
        else:
            days_until_due = (self.duedate - datetime.date.today()).days
            return max(0, self.max_priority - days_until_due)

    def urgency(self) -> int:
        """ Importance measure combining priority and nearness of duedate """
        if self.status is True:
            # completed tasks have lowest urgency
            return -2
        elif self.priority == 0 and self.duedate in [None, ""]:
            # tasks with no priority marking and no duedate get
            # second lowest urgency
            return -1
        else:
            return self.priority + self.duedate_to_added_priority()

    def __int__(self) -> int:
        return self.urgency()

    # Overloads of infix operators

    def __add__(self, other_task: str | Task) -> list[Task]:
        """ task1 + task2 -> list containing both tasks """
        if isinstance(other_task, str):
            other_task = Task(other_task)
        if isinstance(other_task, Task):
            return [self, other_task]
        else:
            raise TypeError(
                f"Task.__add__ requires Task or str, got {type(other_task)}"
            )

    def __xor__(self, _tag: str) -> Task:
        """ self ^ tag1 -> copy of self with additional tag tag1 """
        ret = self.copy()
        ret.tag(_tag)
        return ret

    def __ixor__(self, _tag: str) -> Task:
        """ self ^= tag1 -> append tag tag1 to self.tags """
        self.tag(_tag)
        return self

    def __invert__(self):
        """ ~ task1 -> copy of Task task1 with status reversed """
        ret = self.copy()
        ret.status = not self.status
        return ret

    def __mul__(self, txt: str) -> Task:
        """
        self * txt -> copy of self with txt concatenated to self.notes
        """
        return self.copy().__imul__(txt)

    def __ne__(self, val: int) -> Task:
        """ task1 != val -> Set priority of Task task1 to val """
        return self.set_priority(val)

    def __imul__(self, txt: str) -> Task:
        """ self *= txt -> Concatenate txt to self.notes """
        self.notes.append(Note(txt))
        return self

    def __ilshift__(self, val: int):
        """ task1 <<= val -> decrement duedate of Task task1 by val """
        return self.decrement_duedate(val)

    def __lshift__(self, val):
        """ task1 << val -> copy Task task1 and decrement duedate by val """
        return self.copy().__ilshift__(val)

    def __irshift__(self, val):
        """ task1 >>= val -> increment duedate of Task task1 by val """
        return self.increment_duedate(val)

    def __rshift__(self, val):
        """ task1 >> val -> copy Task task1 and increment duedate by val """
        return self.copy().__irshift__(val)

    def __iand__(self, *date):
        """ task1 &= (year, month, day) -> set duedate of Task task1 """
        return self.set_duedate(*date)

    def __and__(self, *date):
        """ task1 & (year, month, day) -> copy Task task1 and set duedate """
        return self.copy().__iand__(*date)

    def __imatmul__(self, person):
        """ task1 @= person -> assign person to Task task1 """
        return self.assign(person)

    def __matmul__(self, person):
        """ task1 @ person -> assign person to copy of Task task1 """
        return self.copy().__imatmul__(person)

    def __eq__(self, other):
        return self.to_dict() == other.to_dict()

    @staticmethod
    def get_color_profile(dark=DARK_BACKGROUND):
        task_color_sequence = (
            DARK_BACKGROUND_COLOR_SEQUENCE
            if dark
            else LIGHT_BACKGROUND_COLOR_SEQUENCE
        )
        txt_background = None
        return task_color_sequence, txt_background

    def task_color(self, txt, attrs=None, dark=None):
        if dark is None:
            dark = self.dark
        if attrs is None:
            attrs = []
        if self.status is True:
            clr_func = color_func(color=DONE_COLOR, attrs=attrs)
        else:
            colors, txt_background = self.get_color_profile(dark=dark)
            _pry = max(self.urgency(), 0)
            col = colors[min(_pry, len(colors) - 1)]
            clr_func = color_func(
                color=col,
                on_color=txt_background,
                attrs=attrs + (['bold'] if _pry > 0 else [])
            )
        return clr_func(str(txt))

    def next_field(self):
        if len(self.selected_fields) > 0:
            self.selected_fields[0] += 1
        else:
            self.selected_fields = [0]
        self.selected_fields[0] %= self.selected_field_max

    def prev_field(self):
        if len(self.selected_fields) > 0:
            self.selected_fields[0] -= 1
        else:
            self.selected_fields = [-1]
        self.selected_fields[0] %= self.selected_field_max

    def set_status(self, status):
        self.status = status
        if status is True:
            self.date_completed = datetime.datetime.today().date()
        return self

    def x(self):
        """ Set status to True """
        return self.set_status(True)

    def o(self):
        """ Set status to False """
        return self.set_status(False)

    def tag(self, new_tags):
        """ Append new tag or tags to self.tags """
        if isinstance(new_tags, str):
            new_tags = [new_tags]
        for new_tag in new_tags:
            if new_tag not in self.tags:
                self.tags.append(new_tag)
            else:
                print(f"#{new_tag} is already in `.tags`.")
        return self

    def untag(self, _tag=None):
        """ Remove tag from self.tags """
        if _tag is None:
            self.tags.clear()
        if _tag in self.tags:
            self.tags.remove(_tag)
        return self

    def assign(self, _people):
        """ Append person or list of people to self.people """
        _people = str_to_list(_people)
        for person in _people:
            if person not in self.people:
                self.people.append(person)
            else:
                print(f"@{person} is already in `.people`.")
        return self

    def unassign(self, person=None):
        """ Remove person from self.people """
        if person is None:
            self.people.clear()
        if person in self.people:
            self.people.remove(person)
        return self

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, txt):
        self._description = str(txt)

    def set_description(self, txt):
        self.description = txt
        return self

    @property
    def duedate(self) -> datetime.date:
        return self._duedate

    @duedate.setter
    def duedate(self, _duedate: None | list | tuple | str | datetime.date):
        if _duedate is None:
            self._duedate = ""
        elif isinstance(_duedate, (list, tuple)):
            self._duedate = date_obj_from_ints(tuple(_duedate))
        elif isinstance(_duedate, str):
            tmp = date_obj_from_string(_duedate)
            if tmp is not None:
                self._duedate = tmp
        elif isinstance(_duedate, datetime.date):
            self._duedate = _duedate

    def set_duedate(
        self,
        _duedate: None | list | tuple | str | datetime.date
    ) -> Task:
        self.duedate = _duedate
        return self

    due = set_duedate

    def shift_duedate(self, shift_n_days: int) -> Task:
        if isinstance(self.duedate, datetime.date):
            self.duedate = datetime.date.fromordinal(
                self.duedate.toordinal() + shift_n_days
            )
        else:
            self.duedate = datetime.date.today()
        return self

    def increment_duedate(self, val: int = 1) -> Task:
        return self.shift_duedate(val)

    def decrement_duedate(self, val: int = 1) -> Task:
        return self.shift_duedate(-val)

    @property
    def priority(self) -> int:
        return self._priority

    @priority.setter
    def priority(self, _pry: int):
        if isinstance(_pry, int):
            self._priority = max(0, min(_pry, self.max_priority))
        else:
            raise TypeError

    def set_priority(self, _pry: int) -> Task:
        self.priority = _pry
        return self

    def increment_priority(self, val: int = 1) -> Task:
        self.priority = (self.priority + val) % (self.max_priority + 1)
        return self

    def decrement_priority(self, val: int = 1) -> Task:
        self.priority = (self.priority - val) % (self.max_priority + 1)
        return self

    def change_note_selection(self, n: int):
        if len(self.notes) > 0:
            self.notes[self.selected_note_num].selected = False
            self.selected_note_num = n
            self.selected_note_num %= len(self.notes)
            self.notes[self.selected_note_num].selected = True

    def shift_note_selection(self, n: int):
        self.change_note_selection(self.selected_note_num + n)

    def add_note(self, select=True):
        self.notes.append(Note())
        if select:
            self.change_note_selection(len(self.notes) - 1)
            # self.selected_note_num = len(self.notes) - 1

    def select_next_note(self):
        self.shift_note_selection(1)

    def select_prev_note(self):
        self.shift_note_selection(-1)

    def selected_note(self) -> Note:
        if len(self.notes) == 0:
            self.notes.append(Note())
            self.selected_note_num = 0
        return self.notes[self.selected_note_num]

    def edit_notes(self):
        print(f"\nNote {self.selected_note_num}:")
        self.selected_note().edit()
        if len(self.selected_note().text) == 0:
            self.notes.pop(self.selected_note_num)
        else:
            self.expanded = True

    def edit_new_note(self):
        self.add_note(select=True)
        self.edit_notes()

    def edit_selected_field(self):
        if len(self.selected_fields) > 0:
            n = self.selected_fields[0]
            field_name = self.field_names[n]
            if self.field_names[n] == "status":
                print("status:")
                status = self.status
                old_txt = (
                    "X" if status is True
                    else "O" if status is False
                    else status
                )
                new_txt = clipboard_input(
                    old_text=old_txt,
                    old_text_label="current status: ",
                    prompt="new status ('X', 'O', or '/'): "
                )
                if new_txt is None:
                    return
                new_txt = new_txt.upper()
                if new_txt == "X":
                    self.status = True
                elif new_txt == "O":
                    self.status = False
                elif new_txt == "/":
                    self.status = "/"
                return
            if field_name == "notes":
                self.edit_notes()
                return
            self._interactive_edit_field(field_name)

    def _interactive_edit_field(self, field_name: str):
        print(f"\n{field_name}:")
        old_txt = str(self.selected_field_value())
        new_txt = clipboard_input(
            old_text=old_txt,
            old_text_label=f"current {field_name}: ",
            prompt=f"    new {field_name}: "
        )

        if new_txt:
            if field_name == "priority":
                if new_txt.isnumeric():
                    self.priority = int(eval(new_txt))
            elif field_name == "duedate":
                self.set_duedate(new_txt)
            elif field_name == "description":
                self.description = new_txt

    def selected_field_value(self) -> dict | str:
        if len(self.selected_fields) > 0:
            n = self.selected_fields[0]
            return {
                0: self.status,
                1: self.priority,
                2: self.duedate,
                3: self.description,
                4: self.notes
            }.get(n, "")
        return ""

    def to_todotxt(self):
        done_string = "x" if self.status is True else ""
        if self.priority > 0:
            reverse_priority = max(0, self.max_priority - self.priority)
            priority_string = "(" + CAPITAL_ALPHABET[reverse_priority] + ")"
        else:
            priority_string = ""
        date_completed_string = self._date_to_str(self.date_completed)
        date_created_string = self._date_to_str(self.date_created)
        tags_string = " ".join(["@" + tag for tag in self.tags])
        projects_strings = " ".join([
            "+" + project_name for project_name in self.project_names
        ])

        metadata_dict = dict()
        if self.duedate not in [None, ""]:
            metadata_dict["due"] = str(self.duedate)
        if len(self.people) > 0:
            metadata_dict["people"] = self.people
        if len(self.notes) > 0:
            metadata_dict["notes"] = [note.text for note in self.notes]

        metadata_string = " ".join(
            [f"{key}:{val}" for key, val in metadata_dict.items()]
        )

        todo_txt_fields = [
            done_string,
            priority_string,
            date_completed_string,
            date_created_string,
            self.description,
            tags_string,
            projects_strings,
            metadata_string
        ]
        ret = " ".join([field for field in todo_txt_fields if len(field) > 0])
        return ret

    @staticmethod
    def _is_year_month_date(txt: str, sep: str = "-"):
        txt = txt.strip()
        fields = txt.split(sep)
        if not all(field.isnumeric() for field in fields):
            return False
        year, month, day = fields
        if len(year) == 4:
            year = int(year)
        else:
            return False
        if len(month) == 2:
            month = int(month)
        else:
            return False
        if len(day) == 2:
            day = int(day)
        else:
            return False
        return datetime.date(year, month, day)

    @staticmethod
    def _string_list_from_text(txt: str) -> list[str]:
        ret = []
        while len(txt) > 0:
            open_quote = txt[0]
            close_quote_idx = 1 + txt[1:].index(open_quote)
            ret.append(txt[1:close_quote_idx])
            txt = txt[close_quote_idx + 1:].strip(", ")
        return ret

    @staticmethod
    def _extract_metadata_portion_of_line(
        txt: str, metadata_marker: str, end: str = " "
    ):
        if metadata_marker not in txt:
            return txt, ""

        metadata_marker_idx = txt.index(metadata_marker)
        portion_idx_start = metadata_marker_idx + len(metadata_marker)
        remaining_text = txt[portion_idx_start:]
        portion_length = remaining_text.find(end)
        if portion_length < 0:
            portion_length = len(remaining_text)
        portion_idx_end = portion_idx_start + portion_length
        portion_of_line = txt[portion_idx_start:portion_idx_end]
        remainder_of_line = (
            txt[:metadata_marker_idx] + txt[portion_idx_end + len(end):]
        )
        return remainder_of_line, portion_of_line

    def from_todotxt(self, todotxt_line: str):
        if todotxt_line.startswith("x "):
            self.status = True
            todotxt_line = todotxt_line[2:].strip()
            tmp = self._is_year_month_date(todotxt_line[:10])
            if tmp is not False:
                self.date_completed = tmp
                todotxt_line = todotxt_line[10:].strip()
        else:
            self.status = False
        if (
            todotxt_line[0] == "("
            and todotxt_line[1].isupper()
            and todotxt_line[2] == ")"
        ):
            priority_letter = todotxt_line[1]
            try:
                reverse_priority = CAPITAL_ALPHABET.index(priority_letter)
            except ValueError:
                pass
            else:
                self.priority = max(0, self.max_priority - reverse_priority)

            todotxt_line = todotxt_line[3:].strip()

        tmp = self._is_year_month_date(todotxt_line[:10])
        if tmp is not False:
            self.date_created = tmp
            todotxt_line = todotxt_line[10:].strip()

        todotxt_line, notes_portion = self._extract_metadata_portion_of_line(
            todotxt_line, "notes:[", "]"
        )
        if len(notes_portion) > 0:
            notes_text = self._string_list_from_text(notes_portion)
            self.notes = [Note(note_text) for note_text in notes_text]

        todotxt_line, people_portion = self._extract_metadata_portion_of_line(
            todotxt_line, "people:[", "]"
        )
        if len(people_portion) > 0:
            self.people = self._string_list_from_text(people_portion)

        todotxt_line, duedate_portion = self._extract_metadata_portion_of_line(
            todotxt_line, "due:"
        )
        if len(duedate_portion) > 0:
            self.set_duedate(duedate_portion)

        description_txt = ""
        split_txt = todotxt_line.split()
        for word in split_txt:
            if word.startswith("@"):
                self.tags.append(word[1:])
            elif word.startswith("+"):
                self.project_names.append(word[1:])
            else:
                description_txt += word + " "

        self.description = description_txt
        return self



