HINTS_COLUMN_WIDTH = 25
COLUMN_PAD = 2
HINTS = {
    "?": "show/hide shortcuts",
    "q, [Esc]": "quit",
    "s": "save",
    "w": "save as",
    "z": "undo",
    "y": "redo",

    "0-9": "jump to Task #",
    "$": "jump to last Task",
    "↑, ↓/[Tab]": "cycle over Tasks",
    "←, →/[Tab]": "cycle over Task fields",
    "n": "select nth Task",

    "[, ]": "move Task up/down",
    "{, }": "move Task top/bottom",
    ":": "sort by...",
    ";": "auto-sort",

    "+": "add Task",
    "-": "remove Task",

    "[Enter]": "edit",
    "*": "edit note",
    ">, <": "duedate ±1",
    ".": "erase duedate",
    "%": "set duedate",

    "@ (a)": "(un)assign",
    "# (g)": "(un)tag",

    "x": "done",
    "o": "not done",
    "/": "partly done",

    "[Space]": "show/hide notes",
    "d": "show/hide details",
    "t": "tree view",
    "k": "calendar view",


    "h": "show/hide completed",
    "c": "toggle compact view",

    "&": "Task → sub-Project",
    "^": "create new sub-Project",

    "f": "search descriptions",
    "b": "enter source Project"
}
