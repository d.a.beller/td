import td

three_tasks = ["task1", "task2", "task3"]
two_other_tasks = ["task4", "task5"]

class TestProjectCreation:

    def test_empty(self):
        p = td.Project()
        assert p.tasks == []
        assert p.notes == ""

    def test_iadd(self):
        p = td.Project()
        p += "txt"
        assert len(p.tasks) == 1
        t = p.tasks[0]
        assert t == td.Task("txt")

    def test_add(self):
        p = td.Project() + "txt"
        assert len(p.tasks) == 1
        assert p.tasks[0] == td.Task("txt")

    def test_iadd_proj(self):
        p = td.Project(tasks=three_tasks)
        q = td.Project(tasks=two_other_tasks)
        p += q
        assert len(p.tasks) == 5
        assert all(t in p.tasks for t in q.tasks)
        assert all(t in p for t in q)
        assert q in p.subprojects
        assert q in p

    def test_add_proj(self):
        q = td.Project(tasks=two_other_tasks)
        p = td.Project(tasks=three_tasks) + q
        assert len(p.tasks) == 5
        assert all(t in p.tasks for t in q.tasks)
        assert all(t in p for t in q)
        assert q in p.subprojects
        assert q in p

    def test_isub_task(self):
        p = td.Project(tasks=three_tasks)
        p -= p[1]
        assert p.tasks == [three_tasks[0], three_tasks[2]]

    def test_sub_task(self):
        p = td.Project(tasks=three_tasks)
        q = p - p[1]
        assert q.tasks == [three_tasks[0], three_tasks[2]]

    def test_isub_proj(self):
        q = td.Project(tasks=two_other_tasks)
        p = td.Project(tasks=three_tasks) + q
        p -= q
        assert p.tasks == three_tasks
        assert len(p.subprojects) == 1

    def test_isub_proj_iadded(self):
        q = td.Project(tasks=two_other_tasks)
        p = td.Project(tasks=three_tasks)
        p += q
        p -= q
        assert p.tasks == three_tasks
        assert len(p.subprojects) == 0



    def test_iand(self):
        p = td.Project()
        p &= "txt"
        assert len(p.tasks) == 1
        assert p.tasks[0] == td.Task("txt")

    def test_and(self):
        p = td.Project() & "txt"
        assert len(p.tasks) == 1
        assert p.tasks[0] == td.Task("txt")

    def test_imul(self):
        p = td.Project()
        p *= "txt"
        assert p.notes == "txt"

    def test_mul(self):
        p = td.Project() * "txt"
        assert p.notes == "txt"

    def test_imatmul(self):
        p = td.Project(tasks=["task1", "task2", "task3"])
        p @= "Bob"
        assert all(t.people == ["Bob"] for t in p.tasks)

    def test_matmul(self):
        p = td.Project(tasks=["task1", "task2", "task3"]) @ "Bob"
        assert all(t.people == ["Bob"] for t in p.tasks)

    def test_ixor(self):
        p = td.Project(tasks=three_tasks)
        p ^= "txt"
        assert all(t.tags == ["txt"] for t in p.tasks)

    def test_xor(self):
        p = td.Project(tasks=three_tasks) ^ "txt"
        assert all(t.tags == ["txt"] for t in p.tasks)



