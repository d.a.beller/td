# tada

## Overview

An interactive, object-oriented to-do list creator and organizer for people who like to think in Python. Created by Daniel A. Beller, 2021-2024.

## Description

This to-do list utility organizes tasks into projects and allows the user to aggregate, sort, and filter tasks across projects. The goal is to help you decide which task to focus on next from among multiple projects in progress, without losing track of longer-term goals. Using the Python interpreter, *tada* has both a terminal-based interactive mode and an object-oriented interface for Python scripting.


## Installation

From within the *tada* directory, type

        python3 -m pip install .  

## File format

By default, *tada* saves Projects to plaintext ".txt" files where Tasks are listed in the human-readable [todo.txt format](https://github.com/todotxt/todo.txt) standard. We deviate from that intentionally simple format to give the Project name following `# ` on the first line, and subprojects on lines beginning with `+ ` followed by `[*project_name*](*project_filepath*)`, a Markdown-format link. 

Saving to ".json" is possible as an alternative.   

## Classes

* `tada.Task` is a to-do item.
* `tada.Project` is a collection of Tasks, possibly with nested Project objects as subprojects.
* `tada.Note` is optional text attached to a Task or Project.

## Basic usage

### Interactive mode 

Launch *tada* in interactive mode on the default Project by running `tada`.

* In interactive mode, one Task at a time is highlighted, and the up/down arrow keys are used to jump between them.
  * Within the selected Task, left/right arrow keys cycle between editable fields.
  * Press Enter to edit the selected field.
* If not provided a filename, *tada* will look for a file in the location given by `UMBRELLA_PROJECT_FILENAME` in *src/tada/settings.py*, and will create a new Project if that file doesn't exist yet.
* To launch with a different file instead:
    
        tada -f other_project_filename

* Hit `?` to see available keyboard commands for interactive mode.
* Hit `q` to exit interactive mode to the IPython interpreter.


### Python interpreter 

Once you've exited Interactive mode to the IPython interpreter,  the active `Project` is bound to the name `p`. 

* To return to *tada* interactive mode: `p.i`
* To save the `Project`: `p.s`
* To view the `Project`'s tree structure: `p.t`


### Starting from the Python interpreter

You can also just launch the Python interpreter (`python3`) and get started with

    from tada import Project, Task
    p = Project.load(...)

For example, to create a new Project file, you can navigate to any folder and enter into the Python interpreter:

	from tada import Project, Task
	p = Project(name="my_project")  # create new Project
	p += Task("Task 1")             # add a Task to the Project
	p.save()                        # write to 'my_project.txt'

Optionally, you may also add this Project to *tada*'s auto-imported amalgam project with:

	p.add_to_umbrella()

### Print-only mode

To simply print a Project to the terminal:

        tada -p              # default Project
        tada -pf *filename*  # Project at location *filename*

### Editing Task properties in the Python interpreter 

Each Task `t` has these important attributes: 

* `t.status` denotes Task as done, not done, or partially done.
  * Set with `t.set_status([str | bool])` or `t.status = [str | bool]`
  * Shortcuts: `t.x()`, or `t.o()`
* `t.priority` denotes level of importance.
  * Set with `t.set_priority([int])` or `t.priority = [int]`
  * Shortcut: `t != [int]`
* `t.duedate` is a `datetime.date` object storing the Task's due date. 
  * Set with `t.set_duedate("MM/DD/YYYY")` or `t.duedate = datetime.date(YYYY, MM, DD)`
  * Shortcut: `t &= "MM/DD/YYYY"`
* `t.description` stores the actual text of the Task
  * Set with `t.set_description([str])` or `t.description = [str]`
* `t.notes` is a list of Note objects, which can include URLs, further explanations, etc. 
  * Add a note with `from tada import Note; t.notes.append(Note([str]))`
  * Shortcut: `t *= [str]`
* `t.tags` is a list of strings to aid in grouping tasks by topic
  * Add a tag with `t.tag([str])` or `t.tags.append([str])`
  * Shortcut: `t ^= [str]`
  * Remove a tag with `t.untag([str])`
  * Tags are displayed at the end of the task's description.
* `t.people` is a list of strings to indicate to which people this task is assigned
  * Add a person with `t.assign([str])` or `t.people.append([str])`
  * Shortcut: `t @= [str]`
  * Remove an assignment with `t.unassign([str])`
  * These assignments are displayed at the end of a task's description.
* `t.notes` is a list of `tada.Note` objects, each of which has a field `.text` containing a string. 
* `t.date_created` is a `datetime.Date` recording when the Task was created.
* `t.date_completed` is a `datetime.Date` recording when the Task was completed, or `None` if the Task was never completed.  

### Example workflow in the Python interpreter

    from tada import Project, Task
    
    # Make a new project
    p = Project(name='American Literature')
    
    # Make a new task
    t = Task('Turn in essay draft')
    
    # Set task's duedate to September 21, 2022
    t.set_duedate("9/21/2023")   # or `t &= "9/21/2023"`
    
    # Give the task a searchable tag
    t.tag("Faulkner")    # or `t ^= "Faulkner"
    
    # Add task to project
    p += t

    p

Returns:

    American Literature
    │
    └──  0. [ ]           09-21  Turn in essay draft ^Faulkner

Now we can refer to `t` as an element of `p`. For example, to set the `priority` attribute of `t` to `3`, we could do

 	p[0].set_priority(3)   # or `p[0] != 3`

Now, we can add another Task to the Project,

	p += Task("Make bibliography").set_duedate("8/21/2023")

This new Task will be listed second, even though it has an earlier `duedate` than the first task, because of its lower `priority`. We can switch their order with

 	p.sort(by='duedate')

#### Adding tasks 

The pattern 

    p += "some string"

adds a Task to Project `p` with description `"some string"`, status `False`, and all other fields empty. 

Similarly, the pattern 

    p + "some string" 

returns a copy of Project `p` with the new task added.

#### Removing tasks 

To remove the Task at, say, index 1: 

    p.pop(1)

#### Search

We can search among a Project's Tasks for a search string, such as "Faulkner", using `p.find("Faulkner")` or `p % "Faulkner"`. This returns a new Project containing only those tasks with "Faulkner" in one of their fields. Because Tasks in this new Project are references to Task objects also found in the original project `p`, we can operate on the results directly and see the results in `p`. For example, to mark all tasks containing "Faulkner" as "done", we could do 

    for t in p.find("Faulkner"):
      t.x()


## Subprojects 

While a Project is the "highest-level" data structure defined by *tada*, Projects can also be nested through the `subprojects` attribute of the `Project` class. One approach is to "add" two Projects to create a new,  amalgam Project.

	new_project = project1 + project2

Now, `new_project` combines the Tasks from `project1` and `project2` by reference—that is, changes to `Task`s in `new_project` affect their source (`project1` or `project2`).[^1] We can see a disaggregated List of the subprojects using

	new_project.subprojects  # or use alias .sp

or we can view the nesting structure with

	new_project.tree()  # or use alias .t

Similarly, if we do

	project_a += project_b

then `project_b` becomes a subproject of `project_a`, without affecting existing Tasks in `project_a`.

Note that some changes made directly to a subproject `p1` may not be reflected in the superproject `p2` until you run `p2.update()`.

Whenever the `save()` or `.s` method is called on a Project, it is also called recursively on all subprojects.    

[^1]: However, reordering of tasks in one project does not affect their ordering in another.

We can obtain a subproject from its superproject by treating the superproject like a dict and using the subproject's name as the key: 

    super_proj = Project(name="a project")
    super_proj += Project(name="another project", tasks=["A task!"])
    super_proj["another project"]  # returns the subproject 

Alternatively, `super_proj.subprojects` returns a list of the superproject's subprojects, so we could do something like 
    
    sub_proj = [sp for sp in super_proj.subprojects if sp.name == "another project"][0]


### Redundant subproject name disambiguation 

However, with more than one level of nesting, the two approaches differ. The dict-like approach flattens the subproject tree to make all nested subprojects available at "top-level": 

    p1 = Project(name="p1")
    p2 = Project(name="p2")
    p3 = Project(name="p3")
    p1 += p2
    p2 += p3
    p1["p3"]   # returns p3 
    p1.subprojects  # returns a list containing only p2

This can lead to ambiguities if two subprojects at different levels have the same name: 

    p3b = Project(name="p3")
    p1 += p3b
    p1["p3"] is p3b  # True 
    p1["p3"] is p3   # False

(That is, `p1["p3"]` returns the Project with name "p3" found at the highest level in the subproject tree of `p1`.)

But the following is unambiguous:

    p1["p2"]["p3"] is p3  # True 

As a shortcut, nested subproject references can be concatenated into a single string with delimiter "/": 

    p1["p2/p3"] is p3     # True

Note that an error is raised if you try to have multiple projects with the same name at the top level of the subproject tree: 

    p2b = Project(name="p2")
    p1 += p2b  # ValueError: Sorry, a project of name 'p2' is already a subproject at top level. 

