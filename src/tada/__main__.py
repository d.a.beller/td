#!/usr/bin/env python3

import argparse
import asyncio
import sys
import glob
from pathlib import Path
import datetime
from datetime import timedelta

from .task import Task
from .project import Project
from .note import Note
from .utilities import *
from . import settings


def main():

    parser = argparse.ArgumentParser(prog='tada', description="tada: Python hierarchical to-do list manager")
    parser.add_argument(
        '-n', '--new',
        help='create new Project file in current directory',
        default=False,
        action="store_true"
    )
    parser.add_argument(
        '-f', '--filename',
        metavar='FILENAME1[, FILENAME2, ...]',
        type=str,
        nargs="+",
        help='import json file with filename FILENAME etc.',
        default=None
    )
    parser.add_argument(
        '-l', '--local',
        default=False,
        action="store_true",
        help="use first file named td*.json in current directory"
    )
    parser.add_argument(
        '-a', '--add',
        metavar='description',
        type=str,
        nargs="+",
        help="add task with given description",
        default=None
    )
    parser.add_argument(
        '-r', '--remove',
        metavar='n',
        type=int,
        nargs=1,
        help="remove task number n",
        default=None
    )
    parser.add_argument(
        '-s', '--sorted',
        metavar='sortkey',
        type=str,
        nargs=1,
        help='show tasks in project sorted by sortkey',
        default=None
    )
    parser.add_argument(
        '-p', '--printonly',
        action="store_true",
        help='print and exit, avoiding interactive session'
    )
    parser.add_argument(
        '-L', '--light',
        action="store_true",
        help='use colors for light background'
    )
    parser.add_argument(
        '-t', '--tree',
        action="store_true",
        help='print task tree and exit, avoiding interactive session'
    )
    parser.add_argument(
        '-nc', '--no_colors',
        action="store_true",
        help="print with ANSI color codes"
    )

    try:
        _args = parser.parse_args()
    except SystemExit:
        sys.exit()

    def new_local_project(_cwd_string):
        cwd_name = cwd_string.split("/")[-1]
        _filename = "./td.json"
        if Path(_filename).exists():
            raise FileExistsError(
                "Error: A file with the default filename \'td.json\' already "
                f"exists in directory \'{cwd_string}\' so you must specify a "
                f"filename with the -f flag."
            )
        else:
            return Project(name=cwd_name, filename=_filename)

    cwd_string = str(Path.cwd())
    if _args.new:
        proj = new_local_project(cwd_string)
    elif _args.local:
        filenames = glob.glob(cwd_string + "/td*.json")
        if len(filenames) == 0:
            filenames.append(cwd_string + "/td.json")
        filename = filenames[0]
        proj = Project.load(filename, create_if_not_found=True)
    else:
        proj = Project.from_filenames(_args.filename)

    if _args.add is not None:
        add_str = " ".join(_args.add)
        proj += Task(add_str)
        proj.set_selected_tasks([len(proj.tasks)-1])
        _args.printonly = True
        proj.save(max_depth=0)

    if _args.sorted is not None:
        sortkey = _args.sorted[0]
        _args.printonly = True
        print(proj.sorted(by=sortkey))

    proj.colors = not _args.no_colors

    proj.dark = not _args.light

    settings.DARK_BACKGROUND = proj.dark
    return proj, _args
  

async def launch_interactive_project(proj):
    await proj.interactive()


if __name__ == "__main__":
    def today():
        return datetime.datetime.today()
    p, args = main()
    if args.printonly:
        print(p.__repr__(line_spacing=1))
        raise SystemExit(0)
    elif args.tree:
        p.tree()
        raise SystemExit(0)
    else:
        asyncio.run(launch_interactive_project(p))
        print(f"p = Project \"{p.name}\"")
