from __future__ import annotations
import datetime
import re
import readline  # makes `input()` better behaved
from time import sleep

from blessings import Terminal
import pyperclip

from .settings import *


N_SPACES_INDENT = 6


def ansi_code_txt(code: int, endcode: int | None = None) -> callable:
    if endcode is None:
        endcode = code + 20
    return lambda txt: f"\x1b[{code}m{txt}\x1b[{endcode}m"


bold_txt = ansi_code_txt(1)
faint_txt = ansi_code_txt(2)
italic_txt = ansi_code_txt(3)
slow_blink_txt = ansi_code_txt(4)
reversed_txt = ansi_code_txt(7)


def color_func(
    color: list | tuple | int | str | None = None,
    on_color: list | tuple | int | str | None = None,
    attrs: list | tuple | None = None,
    term: Terminal | None = None
) -> callable:
    if term is None:
        term = Terminal()

    if attrs is None:
        attrs = []

    if isinstance(color, str):
        _color_func = getattr(term, color)
    elif isinstance(color, (list, tuple)):
        format_str = "\x1b["
        r, g, b = color
        format_str += f"38;2;{r};{g};{b}"

        def _color_func(txt):
            return format_str + txt + term.normal

    elif isinstance(color, int):
        format_str = term.setaf(color)

        def _color_func(txt):
            return format_str + txt + term.normal
    else:
        def _color_func(txt):
            return txt

    if isinstance(on_color, (tuple, list)):
        format_str = "\x1b["
        r, g, b = on_color
        format_str += f"48;2;{r};{g};{b}"

        def _on_color_func(txt):
            return format_str + txt + term.normal
    elif isinstance(on_color, int):
        _on_color_func = term.on_color(color)
    elif isinstance(on_color, str):
        _on_color_func = getattr(term, "on_" + on_color)
    else:
        def _on_color_func(txt):
            return txt

    if len(attrs) > 0:
        mash_attr_str = "_".join(attrs)
        attr_func = getattr(term, mash_attr_str)
    else:
        def attr_func(txt):
            return txt

    def ret(txt):
        return attr_func(_on_color_func(_color_func(txt)))

    return ret


def selected_style(txt: str) -> str:
    return reversed_txt(txt)


def str_to_list(param: str | list) -> list:
    if isinstance(param, str):
        param = [param]
    return param


def add_year_to_month_day(date_parts: tuple[int]) -> datetime.date:
    date_parts_with_year = (datetime.date.today().year,) + date_parts
    date = datetime.date(*date_parts_with_year)
    if (date - datetime.date.today()).days < 0:
        date_parts_with_year = (datetime.date.today().year + 1,) + date_parts
        date = datetime.date(*date_parts_with_year)
    return date


def date_obj_from_ints(arguments: tuple) -> datetime.date:
    if len(arguments) == 3:
        if len(str(arguments[-1])) == 4:  # assume MM/DD/YYYY
            arguments = (arguments[2], arguments[0], arguments[1])  # YYYY MM DD
        if arguments[1] > 12:  # interchange MM <-> DD
            arguments = list(arguments)
            tmp = arguments[1]
            arguments[1] = arguments[2]
            arguments[2] = tmp
            arguments = tuple(arguments)
        return datetime.date(*arguments)
    elif len(arguments) == 2:
        return add_year_to_month_day(arguments)
    else:
        raise TypeError(
            f"duedate string must be in form \'YYYY MM DD\' or \'MM DD\'; "
            f"got {arguments}"
        )


def date_obj_from_string(
    date_string: str | None | datetime.date
) -> datetime.date | str | None:
    if date_string in ["", " ", None, BLANK_DATE_STR]:
        return ""
    elif isinstance(date_string, datetime.date):
        return date_string
    elif isinstance(date_string, str):
        s = date_string
        s = s.strip()
        s = s.replace(" ", "-")
        s = s.replace("/", "-")
        try:
            date_parts = tuple(int(dp) for dp in s.split("-"))
        except ValueError:
            print(
                f"Error: could not infer date components from"
                f" \'{date_string}\'"
            )
            sleep(MESSAGE_SLEEP_TIME)
            return None
        return date_obj_from_ints(date_parts)
    else:
        raise TypeError(
            "date_obj_from_string parameter \'date_string\' must be None, "
            "str, or datetime.date"
        )


def ansi_escape():
    """ remove ANSI escape sequences """
    return re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')


def clipboard_input(
        prompt: str =         "    new text: ",
        old_text: str = "",
        old_text_label: str = "current text: "
):
    old_text_unformatted = ansi_escape().sub('', old_text)
    old_text_line = faint_txt(old_text_label) + old_text_unformatted
    if len(old_text_unformatted) > 0:
        old_text_line += faint_txt("  -> clipboard")
        # copy old text to clipboard
        pyperclip.copy(old_text_unformatted)
        print(old_text_line)

    # reactivate cursor temporarily
    print(Terminal().normal_cursor(), end="")

    try:
        return input(faint_txt(prompt))
    except KeyboardInterrupt:
        return
