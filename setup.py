import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="tada-dbeller",
    version="0.0.1",
    author="Daniel Beller",
    author_email="d.a.beller@jhu.edu",
    description="To-do list organizer",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/d.a.beller/td",
    # project_urls={
        # "Bug Tracker": "https://github.com/pypa/sampleproject/issues",
    # },
    # classifiers=[
        # "Programming Language :: Python :: 3",
        # "License :: OSI Approved :: MIT License",
        # "Operating System :: OS Independent",
    # ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    install_requires=[
        "sshkeyboard",
        "ipython",
        "markdown",
        "html2text",
        "blessings",
        "nest_asyncio",
        "simple-term-menu",
        "pyperclip"
    ],
    scripts=['scripts/tada']
)
